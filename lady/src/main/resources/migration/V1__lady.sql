--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Debian 11.4-1.pgdg90+1)
-- Dumped by pg_dump version 11.4 (Debian 11.4-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: authority; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.authority (
    id bigint NOT NULL,
    name character varying(255)
);


--
-- Name: authority_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.authority_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: authority_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.authority_id_seq OWNED BY public.authority.id;


--
-- Name: buy; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.buy (
    id bigint NOT NULL,
    create_date date,
    update_date date,
    reason text,
    status character varying(255) NOT NULL,
    total_amount numeric(19,2) NOT NULL,
    feed_id bigint NOT NULL,
    location_id bigint,
    user_buy_id bigint,
    user_delivery_id bigint
);


--
-- Name: buy_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.buy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: buy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.buy_id_seq OWNED BY public.buy.id;


--
-- Name: chart; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.chart (
    id bigint NOT NULL,
    create_date date,
    update_date date,
    feed_id bigint,
    user_id bigint
);


--
-- Name: chart_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.chart_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: chart_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.chart_id_seq OWNED BY public.chart.id;


--
-- Name: feed; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.feed (
    id bigint NOT NULL,
    create_date date,
    update_date date,
    brand character varying(255) NOT NULL,
    categories character varying(255) NOT NULL,
    colors character varying(255),
    description character varying(255),
    expiration_date date,
    out_of_stock boolean,
    price numeric(19,2),
    promotion boolean DEFAULT false,
    rank integer,
    location_id bigint,
    user_id bigint NOT NULL
);


--
-- Name: feed_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.feed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: feed_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.feed_id_seq OWNED BY public.feed.id;


--
-- Name: follow; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.follow (
    id bigint NOT NULL,
    create_date date,
    update_date date,
    follow_id bigint,
    request_id bigint
);


--
-- Name: follow_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.follow_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: follow_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.follow_id_seq OWNED BY public.follow.id;


--
-- Name: location; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.location (
    id bigint NOT NULL,
    create_date date,
    update_date date,
    latitude real NOT NULL,
    longitude real NOT NULL,
    name character varying(255) NOT NULL,
    user_id bigint NOT NULL
);


--
-- Name: location_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: location_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.location_id_seq OWNED BY public.location.id;


--
-- Name: media; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.media (
    id bigint NOT NULL,
    create_date date,
    update_date date,
    display_name character varying(1024) NOT NULL,
    name character varying(250),
    size bigint NOT NULL,
    type character varying(50) NOT NULL,
    url_images character varying(255) NOT NULL,
    feed_id bigint
);


--
-- Name: media_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.media_id_seq OWNED BY public.media.id;


--
-- Name: notification; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.notification (
    id bigint NOT NULL,
    create_date date,
    update_date date,
    action_type character varying(20) NOT NULL,
    touched boolean,
    buy_id bigint,
    feed_id bigint,
    follow_id bigint,
    target_user_id bigint
);


--
-- Name: notification_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.notification_id_seq OWNED BY public.notification.id;


--
-- Name: oauth_access_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_access_token (
    id integer NOT NULL,
    authentication bytea,
    authentication_id character varying(255),
    client_id character varying(255),
    refresh_token character varying(255),
    token bytea,
    token_id character varying(255),
    user_name character varying(255)
);


--
-- Name: oauth_access_token_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.oauth_access_token_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_access_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.oauth_access_token_id_seq OWNED BY public.oauth_access_token.id;


--
-- Name: oauth_client_details; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_client_details (
    id integer NOT NULL,
    access_token_validity integer,
    additional_information character varying(4096),
    authorities character varying(255),
    authorized_grant_types character varying(255),
    autoapprove integer,
    client_id character varying(255),
    client_name character varying(255),
    client_secret character varying(255),
    created timestamp without time zone,
    enabled boolean DEFAULT true,
    refresh_token_validity integer,
    resource_ids character varying(255),
    scope character varying(255),
    uuid character varying(255),
    web_server_redirect_uri character varying(255)
);


--
-- Name: oauth_client_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.oauth_client_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_client_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.oauth_client_details_id_seq OWNED BY public.oauth_client_details.id;


--
-- Name: oauth_client_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_client_token (
    id integer NOT NULL,
    authentication_id character varying(255),
    client_id character varying(255),
    token bytea,
    token_id character varying(255),
    user_name character varying(255)
);


--
-- Name: oauth_client_token_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.oauth_client_token_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_client_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.oauth_client_token_id_seq OWNED BY public.oauth_client_token.id;


--
-- Name: oauth_code; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_code (
    id integer NOT NULL,
    authentication bytea,
    code character varying(255)
);


--
-- Name: oauth_code_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.oauth_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.oauth_code_id_seq OWNED BY public.oauth_code.id;


--
-- Name: oauth_refresh_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_refresh_token (
    id integer NOT NULL,
    authentication bytea,
    token bytea,
    token_id character varying(255)
);


--
-- Name: oauth_refresh_token_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.oauth_refresh_token_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_refresh_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.oauth_refresh_token_id_seq OWNED BY public.oauth_refresh_token.id;


--
-- Name: set; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.set (
    id bigint NOT NULL,
    create_date date,
    update_date date,
    color character varying(255) NOT NULL,
    unit integer NOT NULL,
    buy_id bigint,
    CONSTRAINT set_unit_check CHECK ((unit >= 1))
);


--
-- Name: set_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.set_id_seq OWNED BY public.set.id;


--
-- Name: slide; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.slide (
    id bigint NOT NULL,
    create_date date,
    update_date date,
    index integer NOT NULL,
    slide_home_id bigint
);


--
-- Name: slide_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.slide_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: slide_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.slide_id_seq OWNED BY public.slide.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    bio character varying(255),
    address character varying(255),
    contact character varying(50),
    created_at timestamp without time zone,
    created_by integer,
    dob date,
    email character varying(50),
    is_required_change_password boolean,
    is_visible boolean,
    password character varying(255),
    status character varying(10),
    updated_at timestamp without time zone,
    updated_by integer,
    user_name character varying(20),
    cover_id bigint,
    profile_id bigint
);


--
-- Name: users_authorities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users_authorities (
    user_id bigint NOT NULL,
    authority_id bigint NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: authority id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authority ALTER COLUMN id SET DEFAULT nextval('public.authority_id_seq'::regclass);


--
-- Name: buy id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.buy ALTER COLUMN id SET DEFAULT nextval('public.buy_id_seq'::regclass);


--
-- Name: chart id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.chart ALTER COLUMN id SET DEFAULT nextval('public.chart_id_seq'::regclass);


--
-- Name: feed id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.feed ALTER COLUMN id SET DEFAULT nextval('public.feed_id_seq'::regclass);


--
-- Name: follow id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.follow ALTER COLUMN id SET DEFAULT nextval('public.follow_id_seq'::regclass);


--
-- Name: location id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.location ALTER COLUMN id SET DEFAULT nextval('public.location_id_seq'::regclass);


--
-- Name: media id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.media ALTER COLUMN id SET DEFAULT nextval('public.media_id_seq'::regclass);


--
-- Name: notification id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification ALTER COLUMN id SET DEFAULT nextval('public.notification_id_seq'::regclass);


--
-- Name: oauth_access_token id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_access_token ALTER COLUMN id SET DEFAULT nextval('public.oauth_access_token_id_seq'::regclass);


--
-- Name: oauth_client_details id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_client_details ALTER COLUMN id SET DEFAULT nextval('public.oauth_client_details_id_seq'::regclass);


--
-- Name: oauth_client_token id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_client_token ALTER COLUMN id SET DEFAULT nextval('public.oauth_client_token_id_seq'::regclass);


--
-- Name: oauth_code id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_code ALTER COLUMN id SET DEFAULT nextval('public.oauth_code_id_seq'::regclass);


--
-- Name: oauth_refresh_token id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_refresh_token ALTER COLUMN id SET DEFAULT nextval('public.oauth_refresh_token_id_seq'::regclass);


--
-- Name: set id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.set ALTER COLUMN id SET DEFAULT nextval('public.set_id_seq'::regclass);


--
-- Name: slide id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.slide ALTER COLUMN id SET DEFAULT nextval('public.slide_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: authority; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.authority VALUES (1, 'ADMIN');
INSERT INTO public.authority VALUES (2, 'PRODUCER');
INSERT INTO public.authority VALUES (3, 'USER');
INSERT INTO public.authority VALUES (4, 'DELIVER');


--
-- Data for Name: buy; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: chart; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: feed; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: follow; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: location; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: media; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: oauth_access_token; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: oauth_client_details; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.oauth_client_details VALUES (1, 36000, NULL, 'user', 'password,authorization_code,refresh_token,implicit', NULL, 'app-client-id', NULL, '$2a$10$njt0SdDV5vT/xS3laRSt2usTTA0wIZLzX4E3Lz3uK9MyxbEpBTzdW', NULL, true, 2592000, 'resource-server-rest-api', 'read,write', NULL, NULL);


--
-- Data for Name: oauth_client_token; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: oauth_code; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: oauth_refresh_token; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: set; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: slide; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.users VALUES (1, 'work hard work smart!', 'battambang', '', '2020-11-12 14:51:58.353891', 0, '2020-11-12', 'admin@lady.com.kh', false, false, '$2a$10$njt0SdDV5vT/xS3laRSt2usTTA0wIZLzX4E3Lz3uK9MyxbEpBTzdW', 'active', '2020-11-12 14:51:58.353891', 0, 'admin', NULL, NULL);


--
-- Data for Name: users_authorities; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.users_authorities VALUES (1, 1);


--
-- Name: authority_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.authority_id_seq', 1, false);


--
-- Name: buy_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.buy_id_seq', 1, false);


--
-- Name: chart_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.chart_id_seq', 1, false);


--
-- Name: feed_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.feed_id_seq', 1, false);


--
-- Name: follow_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.follow_id_seq', 1, false);


--
-- Name: location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.location_id_seq', 1, false);


--
-- Name: media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.media_id_seq', 1, false);


--
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.notification_id_seq', 1, false);


--
-- Name: oauth_access_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.oauth_access_token_id_seq', 1, false);


--
-- Name: oauth_client_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.oauth_client_details_id_seq', 1, true);


--
-- Name: oauth_client_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.oauth_client_token_id_seq', 1, false);


--
-- Name: oauth_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.oauth_code_id_seq', 1, false);


--
-- Name: oauth_refresh_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.oauth_refresh_token_id_seq', 1, false);


--
-- Name: set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.set_id_seq', 1, false);


--
-- Name: slide_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.slide_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- Name: authority authority_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authority
    ADD CONSTRAINT authority_pkey PRIMARY KEY (id);


--
-- Name: buy buy_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.buy
    ADD CONSTRAINT buy_pkey PRIMARY KEY (id);


--
-- Name: chart chart_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.chart
    ADD CONSTRAINT chart_pkey PRIMARY KEY (id);


--
-- Name: feed feed_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.feed
    ADD CONSTRAINT feed_pkey PRIMARY KEY (id);


--
-- Name: follow follow_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.follow
    ADD CONSTRAINT follow_pkey PRIMARY KEY (id);


--
-- Name: location location_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id);


--
-- Name: media media_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- Name: notification notification_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_token oauth_access_token_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_access_token
    ADD CONSTRAINT oauth_access_token_pkey PRIMARY KEY (id);


--
-- Name: oauth_client_details oauth_client_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_client_details
    ADD CONSTRAINT oauth_client_details_pkey PRIMARY KEY (id);


--
-- Name: oauth_client_token oauth_client_token_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_client_token
    ADD CONSTRAINT oauth_client_token_pkey PRIMARY KEY (id);


--
-- Name: oauth_code oauth_code_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_code
    ADD CONSTRAINT oauth_code_pkey PRIMARY KEY (id);


--
-- Name: oauth_refresh_token oauth_refresh_token_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_refresh_token
    ADD CONSTRAINT oauth_refresh_token_pkey PRIMARY KEY (id);


--
-- Name: set set_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.set
    ADD CONSTRAINT set_pkey PRIMARY KEY (id);


--
-- Name: slide slide_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.slide
    ADD CONSTRAINT slide_pkey PRIMARY KEY (id);


--
-- Name: users uk_6dotkott2kjsp8vw4d0m25fb7; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT uk_6dotkott2kjsp8vw4d0m25fb7 UNIQUE (email);


--
-- Name: users uk_h9yxaoeuc4t89qbjbxh18ik7x; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT uk_h9yxaoeuc4t89qbjbxh18ik7x UNIQUE (contact);


--
-- Name: slide uk_ji422ex9y6m9oeoooct1w364i; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.slide
    ADD CONSTRAINT uk_ji422ex9y6m9oeoooct1w364i UNIQUE (index);


--
-- Name: users uk_k8d0f2n7n88w1a16yhua64onx; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT uk_k8d0f2n7n88w1a16yhua64onx UNIQUE (user_name);


--
-- Name: authority ukj9vkma9pkho8of1bwxnt5rvgb; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authority
    ADD CONSTRAINT ukj9vkma9pkho8of1bwxnt5rvgb UNIQUE (name);


--
-- Name: users_authorities users_authorities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_authorities
    ADD CONSTRAINT users_authorities_pkey PRIMARY KEY (user_id, authority_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: feed fk3bo1afdex7ce1icmk11mx1ocn; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.feed
    ADD CONSTRAINT fk3bo1afdex7ce1icmk11mx1ocn FOREIGN KEY (location_id) REFERENCES public.location(id);


--
-- Name: location fk55by463ivfy1u1qfylnjswyje; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT fk55by463ivfy1u1qfylnjswyje FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: notification fk6xp5ks8evjiw2wo8txil4mm1m; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk6xp5ks8evjiw2wo8txil4mm1m FOREIGN KEY (buy_id) REFERENCES public.buy(id);


--
-- Name: feed fk7hlh93hvlb1gagl7oyic19kvk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.feed
    ADD CONSTRAINT fk7hlh93hvlb1gagl7oyic19kvk FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: buy fk8048ee3h88pqi8ngsad03sn10; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.buy
    ADD CONSTRAINT fk8048ee3h88pqi8ngsad03sn10 FOREIGN KEY (user_buy_id) REFERENCES public.users(id);


--
-- Name: slide fka3s5kqs4kod6yakap1krj2jnm; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.slide
    ADD CONSTRAINT fka3s5kqs4kod6yakap1krj2jnm FOREIGN KEY (slide_home_id) REFERENCES public.media(id);


--
-- Name: users_authorities fkac1qasdciwqra319h2pa72gh6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_authorities
    ADD CONSTRAINT fkac1qasdciwqra319h2pa72gh6 FOREIGN KEY (authority_id) REFERENCES public.authority(id);


--
-- Name: chart fkbmqwruimyhnolhg65u0tjuwv2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.chart
    ADD CONSTRAINT fkbmqwruimyhnolhg65u0tjuwv2 FOREIGN KEY (feed_id) REFERENCES public.feed(id);


--
-- Name: notification fkd03c1q34a4qpu6j77kltgc733; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fkd03c1q34a4qpu6j77kltgc733 FOREIGN KEY (follow_id) REFERENCES public.follow(id);


--
-- Name: buy fkh2rvf21rlv1c3q9o53cxt76cn; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.buy
    ADD CONSTRAINT fkh2rvf21rlv1c3q9o53cxt76cn FOREIGN KEY (feed_id) REFERENCES public.feed(id);


--
-- Name: users fkhgndkdaintha2ds3frl7sbkiy; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fkhgndkdaintha2ds3frl7sbkiy FOREIGN KEY (profile_id) REFERENCES public.media(id);


--
-- Name: notification fki6w6qoq5xd0xboiakiak3y6r3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fki6w6qoq5xd0xboiakiak3y6r3 FOREIGN KEY (target_user_id) REFERENCES public.users(id);


--
-- Name: notification fkitid6tgnc70scny6gcltfu91u; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fkitid6tgnc70scny6gcltfu91u FOREIGN KEY (feed_id) REFERENCES public.feed(id);


--
-- Name: chart fkjtkatwy5bm44fkvm4a772t232; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.chart
    ADD CONSTRAINT fkjtkatwy5bm44fkvm4a772t232 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: buy fkjxu4scqodke2s3laexnlypti5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.buy
    ADD CONSTRAINT fkjxu4scqodke2s3laexnlypti5 FOREIGN KEY (user_delivery_id) REFERENCES public.users(id);


--
-- Name: follow fko8xbet6mscrwdu7sdumpjl5he; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.follow
    ADD CONSTRAINT fko8xbet6mscrwdu7sdumpjl5he FOREIGN KEY (follow_id) REFERENCES public.users(id);


--
-- Name: users_authorities fkq3lq694rr66e6kpo2h84ad92q; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_authorities
    ADD CONSTRAINT fkq3lq694rr66e6kpo2h84ad92q FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: users fkqrk8j99x2gm7pkqab4oh2r0oa; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fkqrk8j99x2gm7pkqab4oh2r0oa FOREIGN KEY (cover_id) REFERENCES public.media(id);


--
-- Name: media fkr0t8d5ugju7bo9fu5psnlauju; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.media
    ADD CONSTRAINT fkr0t8d5ugju7bo9fu5psnlauju FOREIGN KEY (feed_id) REFERENCES public.feed(id);


--
-- Name: follow fks8v5mhcplxm2kan6qpi2dwpm0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.follow
    ADD CONSTRAINT fks8v5mhcplxm2kan6qpi2dwpm0 FOREIGN KEY (request_id) REFERENCES public.users(id);


--
-- Name: set fkt9tus30yr2r84ow1862i9kt1w; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.set
    ADD CONSTRAINT fkt9tus30yr2r84ow1862i9kt1w FOREIGN KEY (buy_id) REFERENCES public.buy(id);


--
-- Name: buy fktpt8yxnhlgbuoty3167bgh5dp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.buy
    ADD CONSTRAINT fktpt8yxnhlgbuoty3167bgh5dp FOREIGN KEY (location_id) REFERENCES public.location(id);


--
-- PostgreSQL database dump complete
--

