package com.women.lady.repository;

import com.women.lady.entities.chart.Chart;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface ChartRepository extends JpaRepository<Chart, Long> {

    @Query(value = "select * from chart where feed_id = :feedId and user_id = :userId ", nativeQuery = true)
    Optional<Chart> findByFeedIdAndUserId(@NotNull final Long feedId, @NonNull final Long userId);
}
