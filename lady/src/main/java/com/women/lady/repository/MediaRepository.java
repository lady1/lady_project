package com.women.lady.repository;

import com.women.lady.entities.feed.Feed;
import com.women.lady.entities.media.Media;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;

@Repository
public interface MediaRepository extends JpaRepository<Media, Long> {

    @Modifying
    @Query(value = "DELETE FROM media WHERE feed_id = :feedId", nativeQuery = true)
    void deleteByFeedId(@Param("feedId") @NotNull final Long feedId);
}
