package com.women.lady.repository;

import com.women.lady.entities.follow.Follow;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FollowRepository extends JpaRepository<Follow, Long> {

    @Query(value = "select * from follow as f where request_id = :requestId and f.follow_id = :followId ", nativeQuery = true)
    Optional<Follow> findByRequestIdAndFollowId(@NonNull final Long requestId, @NonNull final Long followId);

    List<Follow> findByFollowId(final Long followId);
}
