package com.women.lady.repository;

import com.women.lady.entities.feed.Feed;
import com.women.lady.model.security.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface FeedRepository extends JpaRepository<Feed, Long> {

    @Query("select count(feed) from Feed as feed where feed.createDate = CURRENT_DATE AND feed.user.id = :userId")
    Long countByCurrentDate(@Param("userId") Long userId);

    Optional<Feed> findByIdAndUser(@NotNull final Long id, @NotNull final User user);

    @Query(value = "select *, fo.follow_id as following " +
            "from feed as fe left join follow as fo on fe.user_id = fo.follow_id and fo.request_id = :userId left join media m on m.feed_id = fe.id order by fe.rank desc, fe.create_date desc, following asc ",
            nativeQuery = true)
    Page<Feed> getFeedByPolicy(@NotNull final Long userId, Pageable pageable);
}
