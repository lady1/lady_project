package com.women.lady.common;


import com.women.lady.exception.ApiBaseException;

public class ApiResponse {
    private ApiResponseMessage message;
    private String requestUri;
    private Object data;

    public ApiResponse success() {
        return this.apiResponseMessage("T", "success", "success");
    }

    public ApiResponse success(String code) {
        return this.apiResponseMessage(code, "N/A", "N/A");
    }

    public ApiResponse success(String message, String messageKh) {
        return this.apiResponseMessage("success", message, messageKh);
    }

    public ApiResponse fail(String code) {
        return this.apiResponseMessage(code, "N/A", "N/A");
    }

    public ApiResponse fail(String code, String message) {
        return this.apiResponseMessage(code, message,"N/A");
    }

    public void throwFail(String code) throws ApiBaseException {
        throw new ApiBaseException(code, this.fail(code));
    }
    public ApiResponse throwFail(ApiBaseException e, ApiResponse response) throws ApiBaseException {
        throw new ApiBaseException(e,response);
    }
    public void throwFail(String code,String message) throws ApiBaseException {
        throw new ApiBaseException(code, this.fail(code,message));
    }

    public ApiResponse apiResponseMessage(String code, String message, String messageKh) {
        this.message = new ApiResponseMessage();
        this.message.setCode(code);
        this.message.setMessage(message);
        this.message.setMessageKh(messageKh);
        return this;
    }

    public ApiResponseMessage getMessage() {
        return message;
    }

    public void setMessage(ApiResponseMessage message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }
}
