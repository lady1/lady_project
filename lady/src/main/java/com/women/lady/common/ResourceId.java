package com.women.lady.common;


import lombok.AllArgsConstructor;
import lombok.Getter;
import javax.validation.constraints.NotNull;

@AllArgsConstructor(staticName = "of")
@Getter
public class ResourceId {
    @NotNull
    private Long id;
}
