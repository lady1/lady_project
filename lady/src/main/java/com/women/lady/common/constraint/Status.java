package com.women.lady.common.constraint;

public class Status {
    public static final String CREATE = "create";
    public static final String UPDATE = "update";
    public static final String ACTIVE ="active";
    public static final String INACTIVE ="inactive";
    public static final String SUSPEND ="suspend";
}
