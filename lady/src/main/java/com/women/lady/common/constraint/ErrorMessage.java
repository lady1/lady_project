package com.women.lady.common.constraint;

public class ErrorMessage {
    //success
    public static final String S0000="success";
    public static final String S0001="save success";

    //message info
    public static final String M0000="success";

    //message error
    public static final String E0000 = "Username already existed.";
    public static final String E0001 = "Username not existed.";
    public static final String E0002 = "Mode must be CREATE or UPDATE.";
    public static final String E0003 = "Authority request is incorrect.";
    public static final String E0004 = "User create failed.";
    public static final String E0005 = "User not existed.";
    public static final String E0006 = "User already Existed.";
    public static final String E0007 = "No data found.";
    public static final String E0008 = "Request status not exist.";
    public static final String E0009 = "Parse request error.";
    public static final String E0010 = "Create user not success.";

}
