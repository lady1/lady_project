package com.women.lady.common;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Converter
public class StringListConverter implements AttributeConverter<List<String>, String> {

    @Override
    public String convertToDatabaseColumn(List<String> attribute) {
        if (attribute == null) {
            return null;
        } else if (attribute.isEmpty()) {
            return "";
        } else if (attribute.contains(null)) {
            attribute.removeIf(Objects::isNull);
        }
        return StringUtils.join(attribute, ",");
    }

    @Override
    public List<String> convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        } else if (dbData.trim().length() == 0) {
            return Collections.emptyList();
        }
        return Arrays.asList(dbData.split(","));
    }
}
