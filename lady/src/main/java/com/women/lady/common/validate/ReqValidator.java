package com.women.lady.common.validate;

import com.women.lady.common.ApiResponse;
import com.women.lady.exception.ApiBaseException;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

public class ReqValidator {
    public static void requestKey(String key, JSONObject requestJson, ApiResponse response) throws ApiBaseException {
        if (!requestJson.has(key)|| key.isEmpty()) {
            String message = new StringBuilder("Key")
                    .append(" [ ")
                    .append(key)
                    .append(" ] ")
                    .append("can not be blank.").toString();
            throw new ApiBaseException("fail", message, response);
        }
    }

    public static ApiResponse requestKeys(List<String> keys, JSONObject requestJson, ApiResponse response) throws ApiBaseException {
        validate(keys, requestJson, response);
        return response;
    }

    public static ApiResponse requestHeader(List<String> keys, HttpServletRequest httpServletRequest, ApiResponse response) throws ApiBaseException {
        JSONObject var4 = mapHeaderToJson(httpServletRequest);
        validate(keys, var4, response);
        return response;
    }

    private static JSONObject mapHeaderToJson(HttpServletRequest httpServletRequest) {
        Map<String, Object> map = new HashMap<>();
        Enumeration enumeration = httpServletRequest.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement().toString();
            String value = httpServletRequest.getHeader(key);
            map.put(key, value);
        }
        return new JSONObject(map);
    }

    private static void validate(List<String> keys, JSONObject jsonObject, ApiResponse response) throws ApiBaseException {
        List<String> errors = new ArrayList<>();
        ListIterator iterator = keys.listIterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            if (!jsonObject.has(key)) {
                String message = new StringBuilder("Key")
                        .append(" [ ")
                        .append(key)
                        .append(" ] ")
                        .append("can not be blank.").toString();
                errors.add(message);
            }
        }
        if (!errors.isEmpty()) {
            response.setData(errors);
            throw new ApiBaseException("fail", errors.stream().collect(Collectors.joining("\n")), response);
        }
    }
}
