package com.women.lady.common;

import com.women.lady.base.BaseInterface;

import javax.validation.constraints.NotEmpty;
import java.util.Map;

public class ApiRequest implements BaseInterface {
    private static final long serialVersionUID = 17L;
    @NotEmpty
    private Map<String,Object> data;
    public Map<String, Object> getData() {
        return data;
    }
    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
