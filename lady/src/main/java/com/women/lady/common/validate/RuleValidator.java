package com.women.lady.common.validate;

import com.women.lady.common.ApiResponse;
import com.women.lady.exception.ApiBaseException;
import org.apache.commons.validator.routines.EmailValidator;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RuleValidator {

    public static void usernamePassword(JSONObject requestJson, ApiResponse response) throws ApiBaseException {
        vaUserNamePassword(requestJson, response);
    }

    public static void password(JSONObject requestJson, ApiResponse response) throws ApiBaseException {
        vaUserNamePassword(requestJson, response);
    }

    public static void phone(String key, JSONObject requestJson, ApiResponse response) throws ApiBaseException {
        checkKey(key, requestJson, response);
        vaPhoneNumber(key, requestJson, response);
    }

    public static void email(String key, JSONObject requestJson, ApiResponse response) throws ApiBaseException {
        checkKey(key, requestJson, response);
        vaEmail(key, requestJson, response);
    }

    private static void vaUserNamePassword(JSONObject requestData, ApiResponse response) throws ApiBaseException {
        //=== will update condition here later
        List<String> errors = new ArrayList<>();
        if (requestData.has("username")) {
            var username = requestData.getString("username");
            if (!username.isEmpty()) {
                var length = username.length();
                if (length < 2 || length > 10) {
                    errors.add(new StringBuilder().append("Username must be minimum 2 && maximum 10 characters").toString());
                } else if (!username.matches("[^a-z0-9 ]")) {
                    errors.add("Username must contain number and character.");
                }
            } else
                throw new ApiBaseException("F", "Username can not be empty.", response);
        }

        if (requestData.has("password")) {
            var username = requestData.getString("password");
            if (!username.isEmpty()) {
                var length = username.length();
                if (length < 2 || length > 10) {
                    errors.add(new StringBuilder().append("Password must be minimum 2 && maximum 10 characters").toString());
                } else if (!username.matches("[^a-z0-9 ]")) {
                    errors.add("password must contain number and character.");
                }
            } else
                throw new ApiBaseException("F", "Password can not be empty.", response);
        }

        if (!errors.isEmpty()) {
            new ApiResponse().setData(errors.stream().collect(Collectors.joining("\n")));
            return;
        }
    }

    private static void vaPhoneNumber(String key, JSONObject requestJson, ApiResponse response) throws ApiBaseException {
        String phone = requestJson.getString(key);
        if (phone.length() <= 8 || phone.length() > 11)
            throw new ApiBaseException("F", "phone number length no correct", response);
        else if (!phone.matches("[0-9]+"))
            throw new ApiBaseException("F", "phone number must contain [0-9]", response);
        else if (!phone.startsWith("0"))
            throw new ApiBaseException("F", "phone number must be start with 0", response);
    }

    private static void vaEmail(String key, JSONObject requestJson, ApiResponse response) throws ApiBaseException {
        String email = requestJson.getString(key);
        boolean isValid = EmailValidator.getInstance().isValid(email);
        if (!isValid) {
            throw new ApiBaseException("F", "email not correct format", response);
        }
    }

    private static void checkKey(String key, JSONObject requestJson, ApiResponse response) throws ApiBaseException {
        if (!requestJson.has(key)) {
            throw new ApiBaseException("F", "Key [" + key + "] can't not be blank", response);
        }
    }
}
