package com.women.lady.service.impl;

import com.women.lady.common.ApiRequest;
import com.women.lady.common.ApiResponse;
import com.women.lady.common.constraint.ErrorMessage;
import com.women.lady.common.constraint.Status;
import com.women.lady.common.validate.ReqValidator;
import com.women.lady.exception.ApiBaseException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.model.security.Authority;
import com.women.lady.model.security.User;
import com.women.lady.repository.AuthorityRepository;
import com.women.lady.repository.UserRepository;
import com.women.lady.service.UserManagementService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service("userManagementService")
@Slf4j
public class UserManagementServiceImpl implements UserManagementService {
    private UserRepository userRepository;
    private AuthorityRepository authorityRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserManagementServiceImpl(UserRepository userRepository, AuthorityRepository authorityRepository, PasswordEncoder passwordEncoder) {
        this.authorityRepository = authorityRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public ApiResponse createOrUpdateUser(ApiRequest apiRequest) throws ApiBaseException {
        User user = new User();
        Optional<User> userOpt;

        ApiResponse response = new ApiResponse();
        JSONObject requestData = new JSONObject(apiRequest.getData());
        ReqValidator.requestKey("mode", requestData, response);
        String mode = requestData.getString("mode");
        switch (mode) {
            case Status.CREATE:
                ReqValidator.requestKeys(Arrays.asList(
                        "username",
                        "password",
                        "authorities",
                        "mode"),
                        requestData,
                        response);
                userOpt = userRepository.findByUsername(requestData.getString("username"));
                if (userOpt.isPresent())
                    throw new ApiBaseException("F", ErrorMessage.E0000, response);
                break;
            case Status.UPDATE:
                ReqValidator.requestKeys(Arrays.asList(
                        "username",
                        "password",
                        "authorities",
                        "mode",
                        "id"), requestData, response);
                userOpt = userRepository.findById((long) requestData.getInt("id"));
                if (!userOpt.isPresent())
                    throw new ApiBaseException("F", ErrorMessage.E0001, response);
                break;
            default:
                throw new ApiBaseException("F", ErrorMessage.E0002, response);
        }
        Set<Authority> setAuthorities = new HashSet<>();
        JSONArray reqAuthorizes = requestData.getJSONArray("authorities");
        Iterator iterator = reqAuthorizes.iterator();
        while (iterator.hasNext()) {
            String each = (String) iterator.next();
            Authority authority = authorityRepository.findAuthoritiesByName(each).get();
            if (authority != null)
                setAuthorities.add(authority);
        }

        if (setAuthorities.isEmpty())
            throw new ApiBaseException("F", ErrorMessage.E0003, response);

        if (Status.UPDATE.equals(mode)) {
            user.setId(userOpt.get().getId());
        }
        this.saveOrUpdate(user, requestData, setAuthorities, response);
        response.success();
        return response;
    }

    @Override
    public ApiResponse findUserById(Integer userId) {
        ApiResponse response = new ApiResponse();
        var user = userRepository.findUserById(Long.valueOf(userId));
        if (user != null) {
            response.success();
            response.setData(user);
            return response;
        }
        return response.fail("F", ErrorMessage.E0005);
    }

    @Override
    public ApiResponse findUserDetail(Integer userId) {
        return null;
    }

    @Override
    public ApiResponse findAllUser(ApiRequest apiRequest) {
        ApiResponse response = new ApiResponse();
        JSONObject requestData = new JSONObject(apiRequest.getData());

        var requestPage = requestData.optInt("page", 0);
        var requestSize = requestData.optInt("size", 10);
        Pageable pageable = PageRequest.of(requestPage, requestSize);
        Page<User> page = userRepository.findAll(pageable);
        if (!page.isEmpty()) {
            response.setData(page);
            response.success();
            return response;
        }
        return response.fail("F", ErrorMessage.E0007);
    }

    @Override
    public ApiResponse manageUserStatus(ApiRequest apiRequest) throws ApiBaseException, ResourceNotFoundException {
        ApiResponse response = new ApiResponse();
        JSONObject requestData = new JSONObject(apiRequest.getData());
        ReqValidator.requestKeys(Arrays.asList("id", "status"), requestData, response);

        var userId = requestData.getInt("id");
        var status = requestData.getString("status").toLowerCase();

        var user = userRepository.findUserById((long) userId).orElseThrow(()-> new ResourceNotFoundException(String.format("user id not exist")));
        if (user != null) {
            switch (status) {
                case Status.ACTIVE:
                    user.setStatus(Status.ACTIVE);
                    break;
                case Status.INACTIVE:
                    user.setStatus(Status.INACTIVE);
                    break;
                case Status.SUSPEND:
                    user.setStatus(Status.SUSPEND);
                    break;
                default:
                    throw new ApiBaseException("F", ErrorMessage.E0008, response);
            }
            user.setId((long) userId);
            userRepository.save(user);
            response.success();
        }
        return response.fail("F", ErrorMessage.E0005);
    }

    @Transactional(rollbackFor = JpaSystemException.class)
    void saveOrUpdate(User user, JSONObject requestData, Set<Authority> setAuthorities, ApiResponse response) throws ApiBaseException {
        try {
            user.setUsername(requestData.getString("username"));
            user.setPassword(passwordEncoder.encode(requestData.getString("password")));
            user.setAuthorities(setAuthorities);
            user.setCreatedAt(new Date());
            user.setUpdatedAt(new Date());
            user.setIsRequiredChangePassword(true);
            user.setCreatedBy(7);
            user.setStatus(Status.ACTIVE);
            user.setEmail(requestData.optString("email"));
            user.setAddress(requestData.optString("address", ""));
            user.setBio(requestData.optString("boi", ""));
            user.setDateOfBirth(null);
            user.setIsVisible(true);
//            user.setProfileUrl(requestData.optString("profileUrl"));
//            user.setCoverUrl(requestData.optString("coverUrl"));
            user.setContact(requestData.optString("phoneNumber"));
            userRepository.save(user);
        } catch (JpaSystemException e) {
            log.error("Failed save user:{}", e.getMessage());
            response.throwFail(ErrorMessage.E0010);
        }
    }
}
