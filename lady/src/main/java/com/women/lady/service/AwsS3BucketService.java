package com.women.lady.service;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.apierror.ApiError;
import com.women.lady.exception.apierror.PathError;
import com.women.lady.exception.apierror.ValidationErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.io.File;

@Service
public class AwsS3BucketService {

    private AmazonS3 s3Client;
    @Value("${amazonProperties.bucketName}")
    private String bucketName;
    @Value("${amazonProperties.accessKey}")
    private String accessKey;
    @Value("${amazonProperties.secretKey}")
    private String secretKey;
    @Value("${localEnvironment}")
    private boolean localEnvironment;

    private static final Logger LOG = LoggerFactory.getLogger(AwsS3BucketService.class);

    @PostConstruct
    private void initializeAmazon() {
        final var credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
        s3Client = new AmazonS3Client(credentials);
    }

    /**
     * Upload file to AWS S3
     */
    public void uploadFileTos3bucket(@NotNull final String fileName, @NotNull final File file) throws ResourceBodyException {
        //Stop process when run in local
        if (localEnvironment) return;

        try {
            PutObjectRequest request = new PutObjectRequest(bucketName, fileName, file)
                    .withCannedAcl(CannedAccessControlList.PublicRead);
            s3Client.putObject(request);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new ResourceBodyException(new ApiError(
                    ValidationErrorCode.INTERNAL_ERROR,
                    e.getMessage(),
                    PathError.of("feed")
            ));
        }
    }

    /**
     * Delete file from AWS S3
     */
    public Boolean deleteFileFromS3Bucket(@NotNull final String fileName) {
        //Stop process when run in local
        if (localEnvironment) return true;

        try {
            s3Client.deleteObject(new DeleteObjectRequest(bucketName, fileName));
            return true;
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return false;
        }
    }
}
