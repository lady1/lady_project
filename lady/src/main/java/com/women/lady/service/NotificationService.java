package com.women.lady.service;

import com.women.lady.common.ResourceId;
import com.women.lady.entities.buy.Buy;
import com.women.lady.entities.feed.Feed;
import com.women.lady.entities.follow.Follow;
import com.women.lady.entities.notification.NotificationEnum;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.model.security.User;

import javax.validation.constraints.NotNull;

public interface NotificationService {

    ResourceId markAsRead(@NotNull final Long $notificationId) throws ResourceNotFoundException;
    void alertToUser(@NotNull final Feed $feed);
    void alertToUser(@NotNull final Buy $buy, @NotNull final User $user, @NotNull final NotificationEnum $actionType);
    void alertToUser(@NotNull final Follow $follow, @NotNull final User $user);
}
