package com.women.lady.service;

import com.women.lady.common.ApiRequest;
import com.women.lady.common.ApiResponse;
import com.women.lady.exception.ApiBaseException;

import javax.servlet.http.HttpServletRequest;

public interface LoginService {
    ApiResponse signIn(ApiRequest request, HttpServletRequest httpServletRequest) throws ApiBaseException;
    ApiResponse signUp(ApiRequest request, HttpServletRequest httpServletRequest) throws ApiBaseException;
    ApiResponse forgetPassword(ApiRequest request, HttpServletRequest httpServletRequest) throws ApiBaseException;
}
