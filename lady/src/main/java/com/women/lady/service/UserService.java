package com.women.lady.service;

import com.women.lady.api.form.UserCreateForm;
import com.women.lady.api.form.UserUpdateForm;
import com.women.lady.api.form.UserUpdateFormAdmin;
import com.women.lady.common.ResourceId;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.model.security.User;
import lombok.NonNull;
import org.springframework.web.multipart.MultipartFile;

public interface UserService {
    ResourceId create(@NonNull UserCreateForm $createForm, final MultipartFile $profile, final MultipartFile $cover) throws ResourceBodyException;
    ResourceId update(@NonNull final UserUpdateForm $form, final MultipartFile $profile, final MultipartFile $cover) throws ResourceBodyException, ResourceNotFoundException;
    ResourceId updateByAdmin(@NonNull final UserUpdateFormAdmin $form, final MultipartFile $profile, final MultipartFile $cover, final Long userId) throws ResourceBodyException, ResourceNotFoundException;
    ResourceId follow(@NonNull final Long $id) throws ResourceNotFoundException, ResourceBodyException;
    ResourceId unFollow(@NonNull final Long $id) throws ResourceNotFoundException;
    ResourceId delete(@NonNull final Long $id) throws ResourceNotFoundException;
    User me() throws ResourceNotFoundException;
}
