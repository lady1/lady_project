package com.women.lady.service.impl;

import com.women.lady.api.form.FeedCreateForm;
import com.women.lady.api.form.FeedUpdateForm;
import com.women.lady.common.ResourceId;
import com.women.lady.entities.feed.Feed;
import com.women.lady.entities.location.Location;
import com.women.lady.entities.media.Media;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.exception.apierror.ApiError;
import com.women.lady.exception.apierror.PathError;
import com.women.lady.exception.apierror.ValidationErrorCode;
import com.women.lady.model.security.User;
import com.women.lady.repository.*;
import com.women.lady.service.AwsS3BucketService;
import com.women.lady.service.FeedService;
import com.women.lady.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.validation.constraints.NotNull;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class FeedServiceImpl implements FeedService {

    private static List<String> TYPE_SUPPORT = Arrays.asList("gif", "jpeg", "jpg", "png", "svg", "blob",  "image/gif", "image/jpeg", "image/pjpeg", "image/x-png", "image/png", "image/svg+xml");

    @Value("${amazonProperties.awsS3BucketUrl}")
    private String awsS3BucketUrl;

    private static final Long POST_LIMIT = 300L;
    private static final Long MAX_FILES = 5L;

    private static final Logger LOG = LoggerFactory.getLogger(FeedService.class);

    private final FeedRepository feedRepository;
    private final UserRepository userRepository;
    private final MediaRepository mediaRepository;
    private final LocationRepository locationRepository;
    private final AwsS3BucketService awsS3BucketService;
    private final ChartRepository chartRepository;
    private final BuyRepository buyRepository;
    private final NotificationService notificationService;

    @Autowired
    FeedServiceImpl(final FeedRepository feedRepository,
                    final UserRepository userRepository,
                    final MediaRepository mediaRepository,
                    final LocationRepository locationRepository,
                    final AwsS3BucketService awsS3BucketService,
                    final ChartRepository chartRepository,
                    final BuyRepository buyRepository,
                    final NotificationService notificationService) {
        this.feedRepository = feedRepository;
        this.userRepository = userRepository;
        this.mediaRepository = mediaRepository;
        this.locationRepository = locationRepository;
        this.awsS3BucketService = awsS3BucketService;
        this.chartRepository = chartRepository;
        this.buyRepository = buyRepository;
        this.notificationService = notificationService;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Feed> getFeedWithPolicies(final int start,
                                          final int limit) throws ResourceBodyException {
        if (start < 1 || limit < 1) {
            return Page.empty();
        };
        final var currentUser = User.getUserInfo().getUserDetails();
        final var result = feedRepository.getFeedByPolicy(currentUser.getId(), PageRequest.of(start - 1, limit));
        return result;
    }

    @Override
    @Transactional
    public ResourceId create(@NotNull final FeedCreateForm $Form,
                             @NotNull final List<MultipartFile> $files) throws ResourceBodyException {
        final var form = Feed.map($Form);
        checkPostLimit();
        Location location = null;
        if ($Form.getLocation() != null) {
            location = locationRepository.save(Location.map($Form.getLocation()));
        }
        form.setLocation(location);
        form.setMedias(saveMedias($files));
        Feed result;
        try {
            result = feedRepository.save(form);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            form.getMedias().forEach(media -> {
                awsS3BucketService.deleteFileFromS3Bucket(media.getName());
            });
            throw new ResourceBodyException(
                    new ApiError(
                            ValidationErrorCode.INTERNAL_ERROR,
                            "feed information are invalid.",
                            PathError.of("feed")
                    )
            );
        }
        notificationService.alertToUser(result);
        return ResourceId.of(result.getId());
    }

    @Override
    @Transactional
    public ResourceId update(@NotNull FeedUpdateForm $form,
                             List<MultipartFile> $files,
                             @NotNull Long $id
    ) throws ResourceBodyException, ResourceNotFoundException {
        final var feed = feedRepository.findByIdAndUser($id, User.getUserInfo().getUserDetails())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("id not found %s", $id)));
        final var updateFeed = Feed.map(feed, $form);
        if ($files != null && !$files.isEmpty()) {
            mediaRepository.deleteByFeedId(updateFeed.getId());
            deleteFileFromBucket(feed.getMedias());
            updateFeed.setMedias(saveMedias($files));
        }
        if ($form.getLocation() != null) {
            locationRepository.deleteById(updateFeed.getLocation().getId());
            updateFeed.setLocation(Location.map($form.getLocation()));
        }
        return ResourceId.of(feedRepository.save(updateFeed).getId());
    }

    @Override
    @Transactional
    public ResourceId delete(@NotNull Long $id) throws ResourceNotFoundException {
        final var feed = feedRepository.findByIdAndUser($id, User.getUserInfo().getUserDetails())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("id not found %s", $id)));
        feedRepository.deleteById(feed.getId());
        deleteFileFromBucket(feed.getMedias());
        return ResourceId.of(feed.getId());
    }

    /**
     * delete media from to AWS bucket
     */
    private void deleteFileFromBucket(@NotNull final List<Media> $media) throws ResourceNotFoundException {
        for (final var media : $media) {
            final var status = awsS3BucketService.deleteFileFromS3Bucket(media.getName());
            if (!status) {
                LOG.error("AWS s3 can not delete {}", media.getName());
                throw new ResourceNotFoundException(String.format("AWS s3 can not delete %s", media.getDisplayName()));
            }
        }
    }

    /**
     * Save media and upload to AWS service
     */
    private void checkPostLimit() throws ResourceBodyException {
        final var total = feedRepository.countByCurrentDate(User.getUserInfo().getId());
        if (total >= POST_LIMIT) {
            throw new ResourceBodyException(
                new ApiError(
                    ValidationErrorCode.EXCEED,
                    "You can post 3 time per day",
                    PathError.of("feed")
                )
            );
        }
    }

    /**
     * Save media and upload to AWS service
     */
    private List<Media> saveMedias(@NotNull final List<MultipartFile> $multipartFiles) throws ResourceBodyException {
        final var apiErrors = new ArrayList<ApiError>();
        final var medias = new ArrayList<Media>();
        if ($multipartFiles.size() > MAX_FILES) {
            apiErrors.add(
                    new ApiError(
                            ValidationErrorCode.EXCEED,
                            "file maximum is 5",
                            PathError.of("feed")
                    )
            );
        }
        for (MultipartFile multipartFile : $multipartFiles) {
            final var type = multipartFile.getContentType().toLowerCase();
            if (!TYPE_SUPPORT.contains(type)) {
                apiErrors.add(
                        new ApiError(
                                ValidationErrorCode.MISSING_REFERENCE,
                                "type file are not support",
                                PathError.of("feed"))
                );
            }

            final var filename = generateFileName(multipartFile);
            var media = Media.builder()
                    .displayName(multipartFile.getOriginalFilename())
                    .name(filename)
                    .size(multipartFile.getSize())
                    .type(multipartFile.getContentType())
                    //.user(User.getUserInfo().getUserDetails())
                    .urlImages(awsS3BucketUrl).build();
            media.setCreateDate(new Date());
            media.setUpdateDate(new Date());
            medias.add(mediaRepository.save(media));
            File file = convertMultiPartToFile(multipartFile);
            //upload file to aws s3
            awsS3BucketService.uploadFileTos3bucket(filename, file);
            if (!file.delete()) {
                LOG.warn("File can't delete : {}", multipartFile.getName());
            }
        }
        if (!apiErrors.isEmpty()) {
            throw new ResourceBodyException(apiErrors);
        }
        return medias;
    }

    /**
     * convert MultipartFile to file and store in local
     */
    private File convertMultiPartToFile(MultipartFile $file) {
        try {
            File convFile = new File($file.getOriginalFilename());
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write($file.getBytes());
            //write disk
            BufferedImage img = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
            img.createGraphics().drawImage(ImageIO.read(convFile).getScaledInstance(200, 200, Image.SCALE_SMOOTH),0,0,null);
            ImageIO.write(img, "jpg", convFile);
            return convFile;
        } catch ( Exception e){
            return null;
        }
    }

    /**
     * generate name by combine userId date amd original name
     */
    private String generateFileName(MultipartFile $multiPart) {
        final var name = new Date().getTime() + "-" + $multiPart.getOriginalFilename().replace(" ", "_");
        return String.format("%s_%s", User.getUserInfo().getId(), name);
    }
}
