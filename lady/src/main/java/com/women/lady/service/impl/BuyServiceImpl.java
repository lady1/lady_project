package com.women.lady.service.impl;

import com.women.lady.api.form.BuyCreateForm;
import com.women.lady.api.form.BuyRejectForm;
import com.women.lady.common.ResourceId;
import com.women.lady.entities.buy.Buy;
import com.women.lady.entities.buy.BuyEnum;
import com.women.lady.entities.buy.Set;
import com.women.lady.entities.feed.Feed;
import com.women.lady.entities.notification.NotificationEnum;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.exception.apierror.ApiError;
import com.women.lady.exception.apierror.PathError;
import com.women.lady.exception.apierror.ValidationErrorCode;
import com.women.lady.model.security.User;
import com.women.lady.repository.BuyRepository;
import com.women.lady.repository.FeedRepository;
import com.women.lady.repository.NotificationRepository;
import com.women.lady.repository.UserRepository;
import com.women.lady.service.BuyService;
import com.women.lady.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.lang.module.ResolutionException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BuyServiceImpl implements BuyService {

	private final UserRepository userRepository;
	private final FeedRepository feedRepository;
	private final BuyRepository buyRepository;
	private final NotificationService notificationService;

	@Autowired
	BuyServiceImpl(final UserRepository userRepository,
	               final FeedRepository feedRepository,
	               final BuyRepository buyRepository,
	               final NotificationRepository notificationRepository,
	               final NotificationService notificationService) {

		this.userRepository = userRepository;
		this.feedRepository = feedRepository;
		this.buyRepository = buyRepository;
		this.notificationService = notificationService;
	}

	@Override
	@Transactional
	public ResourceId buy(@NotNull final BuyCreateForm $form,
	                      @NotNull final Long feedId) throws ResourceNotFoundException, ResourceBodyException {

		final var currentUser = userRepository.findById(User.getUserInfo().getUserDetails().getId()).get();
		final var feed = feedRepository.findById(feedId).orElseThrow(() -> new ResourceNotFoundException("product does not exist"));
		final var buy = Buy.map(feed, currentUser, BuyEnum.PENDING, $form.getLocation(), $form.getSets());
		validateFeedForBuy(feed, buy.getSets());
		final var totalAmount = totalAmount(buy.getSets(), feed.getPrice());
		buy.setTotalAmount(totalAmount);
		final var result = buyRepository.save(buy);
		notificationService.alertToUser(result, feed.getUser(), NotificationEnum.PENDING);
		return ResourceId.of(result.getId());
	}

	@Override
	@Transactional
	public ResourceId cancelBuy(@NotNull final Long buyId) throws ResourceNotFoundException, ResourceBodyException {

		final var currentUser = User.getUserInfo().getUserDetails();
		final var buying = buyRepository.findById(buyId)
				.orElseThrow(() -> new ResourceNotFoundException("product does not exist"));
		if (!buying.getUserBuy().getId().equals(currentUser.getId())) {
			throw new ResourceNotFoundException("product id not found");
		}
		if (!buying.getStatus().equals(BuyEnum.PENDING)) {
			throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, String.format("you can not cancel product id %s ", buyId), PathError.of("buy")));
		}
		buying.setStatus(BuyEnum.CANCEL);
		final var result = buyRepository.save(buying);
		notificationService.alertToUser(result, buying.getFeed().getUser(), NotificationEnum.CANCEL);
		return ResourceId.of(result.getId());
	}

	@Override
	@Transactional
	public ResourceId producerAccept(@NotNull final Long $buyId) throws ResourceNotFoundException, ResourceBodyException {

		final var currentUser = User.getUserInfo().getUserDetails();
		final var buying = buyRepository.findById($buyId).orElseThrow(() -> new ResourceNotFoundException("Product does not exist"));
		if (!buying.getFeed().getUser().getId().equals(currentUser.getId())) {
			throw new ResourceNotFoundException("Product id not found");
		}
		if (buying.getStatus().equals(BuyEnum.ACCEPT)) {
			throw new ResourceBodyException(new ApiError(ValidationErrorCode.DUPLICATE, "Product already ACCEPT", PathError.of("buy.accept")));
		} else if (!buying.getStatus().equals(BuyEnum.PENDING)) {
			throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, "Product can not ACCEPT", PathError.of("buy.accept")));
		}
		buying.setStatus(BuyEnum.ACCEPT);
		buying.setUpdateDate(new Date());
		final var result = buyRepository.save(buying);
		notificationService.alertToUser(result, buying.getUserBuy(), NotificationEnum.ACCEPT);
		return ResourceId.of(result.getId());
	}

	@Override
	@Transactional
	public ResourceId producerReject(@NotNull final Long $buyId,
	                                 @NotNull final BuyRejectForm $form) throws ResourceNotFoundException, ResourceBodyException {

		final var currentUser = User.getUserInfo().getUserDetails();
		final var buying = buyRepository.findById($buyId).orElseThrow(() -> new ResourceNotFoundException("product does not exist"));
		if (!buying.getFeed().getUser().getId().equals(currentUser.getId())) {
			throw new ResourceNotFoundException("product id not found");
		}
		if (buying.getStatus().equals(BuyEnum.REJECT)) {
			throw new ResourceBodyException(new ApiError(ValidationErrorCode.DUPLICATE, "Product already REJECT", PathError.of("buy.accept")));
		} else if (!buying.getStatus().equals(BuyEnum.PENDING)) {
			throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, "Product can not REJECT", PathError.of("buy.accept")));
		}
		buying.setStatus(BuyEnum.REJECT);
		buying.setUpdateDate(new Date());
		buying.setReason($form.getReason());
		final var result = buyRepository.save(buying);
		notificationService.alertToUser(result, buying.getUserBuy(), NotificationEnum.REJECT);
		return ResourceId.of(result.getId());
	}

	@Override
	public ResourceId assignDeliver(final @NotNull Long $buyId,
	                                final @NotNull Long $deliverId) throws ResourceNotFoundException, ResourceBodyException {

		final var buying = buyRepository.findById($buyId).orElseThrow(() -> new ResourceNotFoundException("Product does not exist"));
		if (!BuyEnum.ACCEPT.equals(buying.getStatus()) && !BuyEnum.DELIVERY.equals(buying.getStatus())) {
			throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, "Product Assign because status not accept", PathError.of("buy.assign")));
		}
		final var user = userRepository.findUserById($deliverId).orElseThrow(() -> new ResolutionException("Deliver id does not exist."));
		final var isDeliver = user.getAuthorities()
				.stream()
				.filter(authority -> {
					return authority.getName().equalsIgnoreCase("DELIVER");
				})
				.findFirst();

		isDeliver.orElseThrow(
				() -> new ResourceBodyException(new ApiError(ValidationErrorCode.MISSING_REFERENCE, "User not deliver.", PathError.of("buy.assign")))
		);
		if (!user.getIsVisible() || !user.getStatus().equalsIgnoreCase("active")) {
			throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, "user not active or not visible.", PathError.of("buy.assign")));
		}
		buying.setUserDelivery(user);
		buying.setStatus(BuyEnum.DELIVERY);
		buyRepository.save(buying);

		return ResourceId.of(buying.getId());
	}

	@Override
	public ResourceId deliverSuccessful(final @NotNull Long $buyId) throws ResourceNotFoundException, ResourceBodyException {

		final var currentUser = User.getUserInfo().getUserDetails();
		final var buying = buyRepository.findById($buyId).orElseThrow(() -> new ResourceNotFoundException("Product does not exist"));
		if (buying.getUserDelivery().getId() != currentUser.getId()) {
			new ResourceNotFoundException("Product does not exist in your item.");
		}
		if (!buying.getStatus().equals(BuyEnum.DELIVERY)) {
			throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, "Product can not be successful because status not delivery.", PathError.of("buy.successful")));
		}
		buying.setStatus(BuyEnum.SUCCESSFUL);
		buyRepository.save(buying);
		return ResourceId.of(buying.getId());
	}

	@Override
	public ResourceId deliverSuccessfulByAdmin(final @NotNull Long $buyId) throws ResourceNotFoundException, ResourceBodyException {

		final var buying = buyRepository.findById($buyId).orElseThrow(() -> new ResourceNotFoundException("Product does not exist"));
		if (!buying.getStatus().equals(BuyEnum.DELIVERY)) {
			throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, "Producer can not be successful because status not delivery.", PathError.of("buy.successful")));
		}
		buying.setStatus(BuyEnum.SUCCESSFUL);
		buyRepository.save(buying);
		return ResourceId.of(buying.getId());
	}

	public void validateFeedForBuy(@NotNull final Feed $feed,
	                               @NotNull final List<Set> $sets) throws ResourceBodyException {

		final var apiError = new ArrayList<ApiError>();
		if ($feed.isOutOfStock()) {
			apiError.add(new ApiError(ValidationErrorCode.MISSING_REFERENCE, "this product out of stock you can not buy", PathError.of("buy.feedId")));
		}
		if ($feed.getUser().getStatus().equalsIgnoreCase("inactive")) {
			apiError.add(new ApiError(ValidationErrorCode.MISSING_REFERENCE, "you can not buy because seller not active", PathError.of("buy.feedId")));
		}
		// Validate color when use buy
		final var existError = $sets.stream()
				.map(Set::getColor)
				.filter(colorEnum -> {
					return !$feed.getColors().contains(colorEnum.name());
				})
				.collect(Collectors.toList());
		existError.stream()
				.forEach(colorEnum -> {
					apiError.add(new ApiError(ValidationErrorCode.INVALID_FIELD, String.format("%s color does not exist", colorEnum.name()), PathError.of("buy.color")));
				});

		if (!apiError.isEmpty()) {
			throw new ResourceBodyException(apiError);
		}
	}

	private BigDecimal totalAmount(@NotNull final List<Set> $sets,
	                               @NotNull final BigDecimal $price) {

		BigDecimal total = new BigDecimal(0);
		for (Set set : $sets) {
			total = total.add($price.multiply(BigDecimal.valueOf(set.getUnit())));
		}
		return total;
	}
}
