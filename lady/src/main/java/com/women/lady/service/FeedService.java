package com.women.lady.service;

import com.women.lady.api.form.FeedCreateForm;
import com.women.lady.api.form.FeedUpdateForm;
import com.women.lady.common.ResourceId;
import com.women.lady.entities.feed.Feed;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface FeedService {
    ResourceId create(@NotNull final FeedCreateForm $Form, @NotNull final List<MultipartFile> $multipartFiles) throws ResourceBodyException, ResourceNotFoundException;
    ResourceId update(@NotNull final FeedUpdateForm $Form, final List<MultipartFile> $multipartFiles, @NotNull final Long id) throws ResourceBodyException, ResourceNotFoundException;
    ResourceId delete(@NotNull final Long id) throws ResourceNotFoundException;
    Page<Feed> getFeedWithPolicies(final int start, final int limit) throws ResourceBodyException;
}
