package com.women.lady.service.impl;

import com.women.lady.common.ResourceId;
import com.women.lady.entities.buy.Buy;
import com.women.lady.entities.feed.Feed;
import com.women.lady.entities.follow.Follow;
import com.women.lady.entities.notification.Notification;
import com.women.lady.entities.notification.NotificationEnum;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.model.security.User;
import com.women.lady.repository.FollowRepository;
import com.women.lady.repository.NotificationRepository;
import com.women.lady.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private FollowRepository followRepository;

    @Override
    public ResourceId markAsRead(@NotNull Long $notificationId) throws ResourceNotFoundException {
        final var currentUser = User.getUserInfo().getUserDetails();
        final var notification = notificationRepository.findByIdAndTargetUserId($notificationId, currentUser.getId())
                .orElseThrow(() -> new ResourceNotFoundException("notification id not exist"));
        notification.setTouched(true);
        return ResourceId.of(notificationRepository.save(notification).getId());
    }

    @Override
    public void alertToUser(@NotNull final Feed $feed) {
        final var notifications = new ArrayList<Notification>();
        final var userFollowing = followRepository.findByFollowId($feed.getUser().getId())
                .stream()
                .filter(Objects::nonNull)
                .map(Follow::getRequest)
                .collect(Collectors.toList());
        for(User user : userFollowing) {
            final var notification = new Notification();
            notification.setTouched(false);
            notification.setCreateDate(new Date());
            notification.setUpdateDate(new Date());
            notification.setActionType(NotificationEnum.NEWPRODUCT);
            notification.setFeed($feed);
            notification.setTargetUser(user);
            notifications.add(notification);
        }
        notificationRepository.saveAll(notifications);
    }

    @Override
    public void  alertToUser(@NotNull final Buy $buy,
                             @NotNull final User $user,
                             @NotNull final NotificationEnum $actionType) {
        final var notification = new Notification();
        notification.setBuy($buy);
        notification.setFeed($buy.getFeed());
        notification.setTouched(false);
        notification.setTargetUser($user);
        notification.setUpdateDate(new Date());
        notification.setCreateDate(new Date());
        notification.setActionType($actionType);
        notificationRepository.save(notification);
    }

    @Override
    public void alertToUser(@NotNull Follow $follow, @NotNull User user) {
        final var notification = new Notification();
        notification.setFollow($follow);
        notification.setTouched(false);
        notification.setTargetUser(user);
        notification.setUpdateDate(new Date());
        notification.setCreateDate(new Date());
        notification.setActionType(NotificationEnum.FOLLOW);
        notificationRepository.save(notification);
    }
}
