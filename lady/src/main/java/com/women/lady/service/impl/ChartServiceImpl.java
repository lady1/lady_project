package com.women.lady.service.impl;

import com.women.lady.common.ResourceId;
import com.women.lady.entities.chart.Chart;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.exception.apierror.ApiError;
import com.women.lady.exception.apierror.PathError;
import com.women.lady.exception.apierror.ValidationErrorCode;
import com.women.lady.model.security.User;
import com.women.lady.repository.ChartRepository;
import com.women.lady.repository.FeedRepository;
import com.women.lady.repository.UserRepository;
import com.women.lady.service.ChartService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Service
public class ChartServiceImpl implements ChartService {

    private final UserRepository userRepository;
    private final ChartRepository chartRepository;
    private final FeedRepository feedRepository;

    ChartServiceImpl(final UserRepository userRepository,
                     final ChartRepository chartRepository,
                     final FeedRepository feedRepository) {
        this.userRepository = userRepository;
        this.chartRepository = chartRepository;
        this.feedRepository = feedRepository;
    }

    @Override
    @Transactional
    public ResourceId addChart(@NotNull final Long feedId) throws ResourceNotFoundException, ResourceBodyException {
        final var currentUser = userRepository.findById(User.getUserInfo().getUserDetails().getId()).get();
        final var feed = feedRepository.findById(feedId).orElseThrow(() -> new ResourceNotFoundException("product id dose not exist"));
        if(chartRepository.findByFeedIdAndUserId(feed.getId(), currentUser.getId()).isPresent()) {
            throw new ResourceBodyException(new ApiError(ValidationErrorCode.DUPLICATE, String.format("This product id %s already add to chart ", feed.getId()), PathError.of("chart")));
        }
        final var chart = new Chart();
        chart.setFeed(feed);
        chart.setCreateDate(new Date());
        chart.setUpdateDate(new Date());
        final var result = chartRepository.save(chart);
        currentUser.getChart().add(chart);
        userRepository.save(currentUser);
        return ResourceId.of(result.getId());
    }

    @Override
    @Transactional
    public ResourceId removeChart(@NotNull final Long feedId) throws ResourceNotFoundException {
        final var currentUser = userRepository.findById(User.getUserInfo().getUserDetails().getId()).get();
        final var chart = chartRepository.findByFeedIdAndUserId(feedId, currentUser.getId()).orElseThrow(() -> new ResourceNotFoundException("product never add to chart before"));
        chartRepository.delete(chart);
        return ResourceId.of(chart.getId());
    }
}
