package com.women.lady.service;

import com.women.lady.common.ResourceId;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

public interface SlideService {
    ResourceId add(@NotNull final MultipartFile $slideHome, Integer index) throws ResourceBodyException;
    ResourceId remove(@NotNull final Long $id) throws ResourceNotFoundException;
}
