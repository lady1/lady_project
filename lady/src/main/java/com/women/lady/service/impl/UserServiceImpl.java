package com.women.lady.service.impl;

import com.women.lady.api.form.UserCreateForm;
import com.women.lady.api.form.UserUpdateForm;
import com.women.lady.api.form.UserUpdateFormAdmin;
import com.women.lady.common.ResourceId;
import com.women.lady.entities.follow.Follow;
import com.women.lady.entities.media.Media;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.exception.apierror.ApiError;
import com.women.lady.exception.apierror.PathError;
import com.women.lady.exception.apierror.ValidationErrorCode;
import com.women.lady.model.security.Authority;
import com.women.lady.model.security.User;
import com.women.lady.repository.*;
import com.women.lady.service.AwsS3BucketService;
import com.women.lady.service.FeedService;
import com.women.lady.service.NotificationService;
import com.women.lady.service.UserService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.validation.constraints.NotNull;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.*;
import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private static List<String> TYPE_SUPPORT = Arrays.asList("gif", "jpeg", "jpg", "png", "svg", "blob",  "image/gif", "image/jpeg", "image/pjpeg", "image/x-png", "image/png", "image/svg+xml");

    @Value("${amazonProperties.awsS3BucketUrl}")
    private String awsS3BucketUrl;

    private static final Logger LOG = LoggerFactory.getLogger(FeedService.class);

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final PasswordEncoder passwordEncoder;
    private final MediaRepository mediaRepository;
    private final LocationRepository locationRepository;
    private final AwsS3BucketService awsS3BucketService;
    private final FollowRepository followRepository;
    private final NotificationService notificationService;

    @Autowired
    public UserServiceImpl(final UserRepository $userRepository,
                           final AuthorityRepository $authorityRepository,
                           final PasswordEncoder $passwordEncoder,
                           final MediaRepository $mediaRepository,
                           final LocationRepository $locationRepository,
                           final AwsS3BucketService $awsS3BucketService,
                           final FollowRepository followRepository,
                           final NotificationService notificationService) {
        this.authorityRepository = $authorityRepository;
        this.userRepository = $userRepository;
        this.passwordEncoder = $passwordEncoder;
        this.mediaRepository = $mediaRepository;
        this.locationRepository = $locationRepository;
        this.awsS3BucketService = $awsS3BucketService;
        this.followRepository = followRepository;
        this.notificationService = notificationService;
    }

    @Override
    @Transactional
    public ResourceId follow(@NonNull final Long $id) throws ResourceNotFoundException, ResourceBodyException {
        final var currentUser = userRepository.findUserById(User.getUserInfo().getId()).orElseThrow(()->new ResourceNotFoundException("current user not exist"));
        final var target = userRepository.findUserById($id).orElseThrow(()->new ResourceNotFoundException("user not exist"));
        final var findByRequestIdAndFollowId = followRepository.findByRequestIdAndFollowId(currentUser.getId(), target.getId());
        if (findByRequestIdAndFollowId.isPresent()) {
            throw new ResourceBodyException(new ApiError(ValidationErrorCode.DUPLICATE, "already followed", PathError.of("follow")));
        }
        final var follow = new Follow();
        follow.setFollow(target);
        follow.setCreateDate(new Date());
        follow.setUpdateDate(new Date());
        final var result = followRepository.save(follow);
        currentUser.getFollowing().add(result);
        userRepository.save(currentUser);
        notificationService.alertToUser(result, target);
        return ResourceId.of(result.getId());
    }

    @Override
    @Transactional
    public ResourceId unFollow(@NonNull final Long $id) throws ResourceNotFoundException {
        final var currentUser = userRepository.findUserById(User.getUserInfo().getId()).orElseThrow(()->new ResourceNotFoundException("current user not exist"));
        final var target = userRepository.findUserById($id).orElseThrow(()->new ResourceNotFoundException("user not exist"));
        final var follow = followRepository.findByRequestIdAndFollowId(currentUser.getId(), target.getId()).orElseThrow(()->new ResourceNotFoundException("you are not follow before"));
        followRepository.deleteById(follow.getId());
        return ResourceId.of(follow.getId());
    }

    @Override
    public ResourceId delete(@NonNull Long $id) throws ResourceNotFoundException {
        final var user = userRepository.findUserById($id)
                .orElseThrow(() -> new ResourceNotFoundException("user does not exist"));
        user.setIsVisible(false);
        userRepository.save(user);
        return ResourceId.of(user.getId());
    }

    @Override
    @Transactional
    public ResourceId create(@NonNull UserCreateForm $createForm,
                             final MultipartFile $profile,
                             final MultipartFile $cover) throws ResourceBodyException {
        final var user = User.map($createForm);
        final var authoritiesByName = authorityRepository.findAuthoritiesByName($createForm.getAuthorities())
                .orElseThrow(()-> new ResourceBodyException(new ApiError(ValidationErrorCode.MISSING_FIELD, "authorities not exit", PathError.of("user.authorities"))));
        if (authoritiesByName.getName().equalsIgnoreCase("ADMIN")) {
            try {
                if (User.getUserInfo() != null) {
                    final var role = User.getUserInfo().getUserDetails().getAuthorities().stream()
                            .filter(Objects::nonNull)
                            .filter(authority -> {return authority.getName().equalsIgnoreCase("ADMIN");})
                            .findFirst();
                    if (role != null) {
                        user.setIsVisible(false);
                        user.setStatus("inActive");
                    } else {
                        throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, "authorities not exit", PathError.of("user.authorities")));
                    }
                }
            } catch (Exception e) {
                throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, "authorities not exit", PathError.of("user.authorities")));
            }
        } else if (authoritiesByName.getName().equalsIgnoreCase("PRODUCER") ||
                authoritiesByName.getName().equalsIgnoreCase("DELIVER")) {
            user.setIsVisible(false);
            user.setStatus("inActive");
        } else {
            user.setIsVisible(true);
            user.setStatus("active");
        }
        user.setAuthorities(Set.of(authoritiesByName));
        user.setPassword(passwordEncoder.encode($createForm.getPassword()));
        if ($profile != null) {
            user.setProfile(saveMedias($profile));
        }
        if ($cover != null) {
            user.setCover(saveMedias($cover));
        }
        validateDuplicateValue(user.getUsername(), user.getContact(), user.getEmail());
        return ResourceId.of(userRepository.save(user).getId());
    }

    @Override
    @Transactional
    public ResourceId update(@NonNull final UserUpdateForm $form,
                             final MultipartFile $profile,
                             final MultipartFile $cover) throws ResourceBodyException, ResourceNotFoundException {
        final var currentUser = userRepository.findUserById(User.getUserInfo().getId())
                .orElseThrow(()-> new ResourceNotFoundException(String.format("user id not exist")));
        validateDuplicateValue($form.getUsername(), $form.getContact(), $form.getEmail());
        final var updateUser = User.map(currentUser, $form);
        if ($form.getPassword() != null) {
            final var encode = passwordEncoder.encode($form.getPassword());
            if (encode.equals(currentUser.getPassword())) {
                throw new ResourceBodyException(new ApiError(ValidationErrorCode.DUPLICATE, "Please do not use old password", PathError.of("user.password")));
            }
            updateUser.setPassword(encode);
        }
        if ($profile != null) {
            if (currentUser.getProfile() != null) {
                deleteFileFromBucket(currentUser.getProfile());
            }
            updateUser.setProfile(saveMedias($profile));
        }
        if ($cover != null) {
            if (currentUser.getCover() != null) {
                deleteFileFromBucket(currentUser.getCover());
            }
            updateUser.setCover(saveMedias($cover));
        }
        return ResourceId.of(userRepository.save(updateUser).getId());
    }

    @Override
    @Transactional
    public ResourceId updateByAdmin(@NonNull final UserUpdateFormAdmin $form,
                                    final MultipartFile $profile,
                                    final MultipartFile $cover,
                                    final Long $userId) throws ResourceBodyException, ResourceNotFoundException {
        final var currentUser = userRepository.findUserById($userId)
                .orElseThrow(()-> new ResourceNotFoundException(String.format("user id not exist %s", $userId)));
        validateDuplicateValue($form.getUsername(), $form.getContact(), $form.getEmail());
        final var updateUser = User.map(currentUser, $form);
        if ($form.getPassword() != null) {
            final var encode = passwordEncoder.encode($form.getPassword());
            if (encode.equals(currentUser.getPassword())) {
                throw new ResourceBodyException(new ApiError(ValidationErrorCode.DUPLICATE, "Please do not use old password", PathError.of("user.password")));
            }
            updateUser.setPassword(encode);
        }
        if ($profile != null) {
            if (currentUser.getProfile() != null) {
                deleteFileFromBucket(currentUser.getProfile());
            }
            updateUser.setProfile(saveMedias($profile));
        }
        if ($cover != null) {
            if (currentUser.getCover() != null) {
                deleteFileFromBucket(currentUser.getCover());
            }
            updateUser.setCover(saveMedias($cover));
        }
        return ResourceId.of(userRepository.save(updateUser).getId());
    }

    @Override
    public User me() throws ResourceNotFoundException {
        return userRepository.findUserById(User.getUserInfo().getId())
                .orElseThrow(()-> new ResourceNotFoundException(String.format("user id not exist")));
    }

    private void validateDuplicateValue(final String $username,
                                        final String $contact,
                                        final String $email) throws ResourceBodyException {
        final var apiErrors = new ArrayList<ApiError>();
        var tt = userRepository.findByUsername($username);
        if ($username != null &&
            userRepository.findByUsername($username).isPresent()) {
            apiErrors.add(new ApiError(ValidationErrorCode.DUPLICATE, "User name already exist", PathError.of("user.username")));
        }
        if ($contact != null &&
            userRepository.findByContact($contact).isPresent()) {
            apiErrors.add(new ApiError(ValidationErrorCode.DUPLICATE, "contact already exist", PathError.of("user.contact")));
        }
        if ($email != null &&
            userRepository.findByEmail($email).isPresent()) {
            apiErrors.add(new ApiError(ValidationErrorCode.DUPLICATE, "email already exist", PathError.of("user.email")));
        }
        if (!apiErrors.isEmpty()) {
            throw new ResourceBodyException(apiErrors);
        }
    }

    /**
     * Save media and upload to AWS service
     */
    private Media saveMedias(@NotNull final MultipartFile $multipartFiles) throws ResourceBodyException {
        final var apiErrors = new ArrayList<ApiError>();
        final var type = $multipartFiles.getContentType().toLowerCase();
        if (!TYPE_SUPPORT.contains(type)) {
            apiErrors.add(
                    new ApiError(
                            ValidationErrorCode.MISSING_REFERENCE,
                            "type file are not support",
                            PathError.of("feed"))
            );
        }

        if (!apiErrors.isEmpty()) {
            throw new ResourceBodyException(apiErrors);
        }

        final var filename = generateFileName($multipartFiles);
        var media = Media.builder()
                .displayName($multipartFiles.getOriginalFilename())
                .name(filename)
                .size($multipartFiles.getSize())
                .type($multipartFiles.getContentType())
                //.user(user)
                .urlImages(awsS3BucketUrl).build();
        media.setCreateDate(new Date());
        media.setUpdateDate(new Date());
        File file = convertMultiPartToFile($multipartFiles);
        //upload file to aws s3
        awsS3BucketService.uploadFileTos3bucket(filename, file);
        if (!file.delete()) {
            LOG.warn("File can't delete : {}", $multipartFiles.getName());
        }

        return media;
    }

    /**
     * convert MultipartFile to file and store in local
     */
    private File convertMultiPartToFile(MultipartFile $file) {
        try {
            File convFile = new File($file.getOriginalFilename());
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write($file.getBytes());
            //write disk
            BufferedImage img = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
            img.createGraphics().drawImage(ImageIO.read(convFile).getScaledInstance(200, 200, Image.SCALE_SMOOTH),0,0,null);
            ImageIO.write(img, "jpg", convFile);
            return convFile;
        } catch ( Exception e){
            return null;
        }
    }

    /**
     * generate name by combine userId date amd original name
     */
    private String generateFileName(MultipartFile $multiPart) {
        return new Date().getTime() + "-" + $multiPart.getOriginalFilename().replace(" ", "_");
    }

    /**
     * delete media from to AWS bucket
     */
    private void deleteFileFromBucket(@NotNull final Media $media) throws ResourceNotFoundException {
        final var status = awsS3BucketService.deleteFileFromS3Bucket($media.getName());
        if (!status) {
            LOG.error("AWS s3 can not delete {}", $media.getName());
            throw new ResourceNotFoundException(String.format("AWS s3 can not delete %s", $media.getDisplayName()));
        }
    }
}
