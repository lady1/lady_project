package com.women.lady.service.impl;

import com.women.lady.common.ResourceId;
import com.women.lady.entities.media.Media;
import com.women.lady.entities.slide.Slide;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.exception.apierror.ApiError;
import com.women.lady.exception.apierror.PathError;
import com.women.lady.exception.apierror.ValidationErrorCode;
import com.women.lady.model.security.User;
import com.women.lady.repository.MediaRepository;
import com.women.lady.repository.SlideRepository;
import com.women.lady.service.AwsS3BucketService;
import com.women.lady.service.FeedService;
import com.women.lady.service.SlideService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.validation.constraints.NotNull;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class SlideServiceImpl implements SlideService {


    private static List<String> TYPE_SUPPORT = Arrays.asList("gif", "jpeg", "jpg", "png", "svg", "blob",  "image/gif", "image/jpeg", "image/pjpeg", "image/x-png", "image/png", "image/svg+xml");
    private static final Logger LOG = LoggerFactory.getLogger(FeedService.class);
    @Value("${amazonProperties.awsS3BucketUrl}")
    private String awsS3BucketUrl;

    private final AwsS3BucketService awsS3BucketService;
    private final MediaRepository mediaRepository;
    private final SlideRepository slideRepository;

    SlideServiceImpl(final AwsS3BucketService awsS3BucketService,
                     final MediaRepository mediaRepository,
                     final SlideRepository slideRepository) {
        this.awsS3BucketService = awsS3BucketService;
        this.mediaRepository = mediaRepository;
        this.slideRepository = slideRepository;
    }

    @Override
    @Transactional
    public ResourceId add(@NotNull MultipartFile $slideHome, Integer index) throws ResourceBodyException {
        final var slides = slideRepository.findAll();
        int indexSlide = 0;
        if (index == null || index == 0) {
            indexSlide = slides.stream()
                    .mapToInt(Slide::getIndex)
                    .max()
                    .orElse(0);
            //find last index and plus 1
            indexSlide ++;
        } else {
            final var duplicate = slides.stream()
                    .filter(slide -> slide.getIndex() == index)
                    .findFirst();
            if (duplicate.isPresent()) {
                throw new ResourceBodyException(
                        new ApiError(ValidationErrorCode.DUPLICATE,
                                     "index already exist.",
                                     PathError.of("index")));
            }
            indexSlide = index;
        }
        // default should last index slide
        final var slide = Slide.of(indexSlide, saveMedias($slideHome));
        try {
            final var result = slideRepository.save(slide);
            return ResourceId.of(result.getId());
        } catch (Exception e) {
            LOG.error(e.getMessage());
            awsS3BucketService.deleteFileFromS3Bucket(slide.getSlideHome().getName());
            throw new ResourceBodyException(
                    new ApiError(ValidationErrorCode.INTERNAL_ERROR,
                                 "Your images invalid.",
                                 PathError.of("file")));
        }
    }

    @Override
    @Transactional
    public ResourceId remove(@NotNull final Long $id) throws ResourceNotFoundException {
        final var slide = slideRepository.findById($id).orElseThrow(() -> new ResourceNotFoundException(String.format("remove file id not exist %s", $id)));
        slideRepository.delete(slide);
        awsS3BucketService.deleteFileFromS3Bucket(slide.getSlideHome().getName());
        return ResourceId.of(slide.getId());
    }

        /**
         * Save media and upload to AWS service
         */
    private Media saveMedias(@NotNull final MultipartFile $multipartFile) throws ResourceBodyException {
        final var apiErrors = new ArrayList<ApiError>();
            final var type = $multipartFile.getContentType().toLowerCase();
            if (!TYPE_SUPPORT.contains(type)) {
                apiErrors.add(
                        new ApiError(
                                ValidationErrorCode.MISSING_REFERENCE,
                                "type file are not support",
                                PathError.of("feed"))
                );
            }

            final var filename = generateFileName($multipartFile);
            var media = Media.builder()
                    .displayName($multipartFile.getOriginalFilename())
                    .name(filename)
                    .size($multipartFile.getSize())
                    .type($multipartFile.getContentType())
                    //.user(User.getUserInfo().getUserDetails())
                    .urlImages(awsS3BucketUrl).build();
            media.setCreateDate(new Date());
            media.setUpdateDate(new Date());
            File file = convertMultiPartToFile($multipartFile);
            //upload file to aws s3
            awsS3BucketService.uploadFileTos3bucket(filename, file);
            if (!file.delete()) {
                LOG.warn("File can't delete : {}", $multipartFile.getName());
            }
        return media;
    }

    /**
     * convert MultipartFile to file and store in local
     */
    private File convertMultiPartToFile(MultipartFile $file) {
        try {
            File convFile = new File($file.getOriginalFilename());
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write($file.getBytes());
            //write disk
            BufferedImage img = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
            img.createGraphics().drawImage(ImageIO.read(convFile).getScaledInstance(200, 200, Image.SCALE_SMOOTH),0,0,null);
            ImageIO.write(img, "jpg", convFile);
            return convFile;
        } catch ( Exception e){
            return null;
        }
    }

    /**
     * generate name by combine userId date amd original name
     */
    private String generateFileName(MultipartFile $multiPart) {
        final var name = new Date().getTime() + "-" + $multiPart.getOriginalFilename().replace(" ", "_");
        return String.format("%s_%s", User.getUserInfo().getId(), name);
    }
}
