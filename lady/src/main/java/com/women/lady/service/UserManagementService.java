package com.women.lady.service;

import com.women.lady.common.ApiRequest;
import com.women.lady.common.ApiResponse;
import com.women.lady.exception.ApiBaseException;
import com.women.lady.exception.Resource.ResourceNotFoundException;

public interface UserManagementService {
    ApiResponse createOrUpdateUser(ApiRequest apiRequest) throws ApiBaseException;

    ApiResponse manageUserStatus(ApiRequest apiRequest) throws ApiBaseException, ResourceNotFoundException;

    ApiResponse findUserById(Integer userId);

    ApiResponse findUserDetail(Integer userId);

    ApiResponse findAllUser(ApiRequest apiRequest) throws ApiBaseException;

}
