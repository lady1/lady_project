package com.women.lady.service;

import com.women.lady.api.form.BuyCreateForm;
import com.women.lady.api.form.BuyRejectForm;
import com.women.lady.api.form.FeedCreateForm;
import com.women.lady.api.form.FeedUpdateForm;
import com.women.lady.common.ResourceId;
import com.women.lady.entities.feed.Feed;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface BuyService {
    ResourceId buy(@NotNull final BuyCreateForm $form, @NotNull final Long feedId) throws ResourceNotFoundException, ResourceBodyException;
    ResourceId cancelBuy(@NotNull final Long buyId) throws ResourceNotFoundException, ResourceBodyException;
    ResourceId producerAccept(@NotNull final Long $buyId) throws ResourceNotFoundException, ResourceBodyException;
    ResourceId producerReject(@NotNull final Long $buyId, @NotNull final BuyRejectForm $form) throws ResourceNotFoundException, ResourceBodyException;
    ResourceId assignDeliver(@NotNull final Long $buyId, @NotNull final Long $deliverId) throws ResourceNotFoundException, ResourceBodyException;
    ResourceId deliverSuccessful(@NotNull final Long $buyId) throws ResourceNotFoundException, ResourceBodyException;
    ResourceId deliverSuccessfulByAdmin(@NotNull final Long $buyId) throws ResourceNotFoundException, ResourceBodyException;

}
