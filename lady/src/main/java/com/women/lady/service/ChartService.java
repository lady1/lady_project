package com.women.lady.service;

import com.women.lady.common.ResourceId;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import javax.validation.constraints.NotNull;

public interface ChartService {
    ResourceId addChart(@NotNull final Long feedId) throws ResourceNotFoundException, ResourceBodyException;
    ResourceId removeChart(@NotNull final Long feedId) throws ResourceNotFoundException;
}
