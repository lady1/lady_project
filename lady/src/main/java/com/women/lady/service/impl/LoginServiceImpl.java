package com.women.lady.service.impl;

import com.women.lady.common.ApiRequest;
import com.women.lady.common.ApiResponse;
import com.women.lady.exception.ApiBaseException;
import com.women.lady.service.LoginService;
import org.springframework.stereotype.Service;


import javax.servlet.http.HttpServletRequest;

@Service("loginService")
public class LoginServiceImpl implements LoginService {

    @Override
    public ApiResponse signIn(ApiRequest request, HttpServletRequest httpServletRequest) throws ApiBaseException {
        return null;
    }

    @Override
    public ApiResponse signUp(ApiRequest request, HttpServletRequest httpServletRequest) throws ApiBaseException {
        return null;
    }

    @Override
    public ApiResponse forgetPassword(ApiRequest request, HttpServletRequest httpServletRequest) throws ApiBaseException {
        return null;
    }
}
