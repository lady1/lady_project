package com.women.lady.exception.apierror;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class PathFailedResponse extends ResponseEntity<PathFailedResponse.Payload> {
    public PathFailedResponse(@NonNull final String _message)
    {
        super(new Payload(HttpStatus.NOT_FOUND.value(), _message), HttpStatus.NOT_FOUND);
    }

    @Value
    @AllArgsConstructor
    @EqualsAndHashCode
    public static class Payload
    {
        private Payload()
        {
            this.status = null;
            this.message = null;
        }
        @NonNull
        private Integer status;

        @NonNull
        private String message;
    }
}
