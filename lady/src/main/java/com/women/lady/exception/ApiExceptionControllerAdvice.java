package com.women.lady.exception;

import com.women.lady.common.ApiResponse;
import com.women.lady.common.ApiResponseMessage;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.exception.apierror.ApiError;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.apierror.PathError;
import com.women.lady.exception.apierror.PathFailedResponse;
import com.women.lady.exception.apierror.BodyFailedResponse;
import com.women.lady.exception.apierror.ValidationErrorCode;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ApiExceptionControllerAdvice {

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(ApiBaseException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ApiResponse handleApiRequestException(ApiBaseException e, HttpServletRequest request){
        ApiResponse response = new ApiResponse();
        ApiResponseMessage message = new ApiResponseMessage();
        response.setRequestUri(request.getRequestURI());
        message.setCode(e.response.getMessage().getCode());
        message.setMessage(e.response.getMessage().getMessage());
        message.setMessageKh(e.response.getMessage().getMessageKh());
        response.setMessage(message);
        return response;
    }

    @ExceptionHandler(BindException.class)
    public BodyFailedResponse errorFormBind(final BindException $e) {
        List<ApiError> errors = $e.getBindingResult().getFieldErrors().stream()
                .map(ApiError::getInstance)
                .collect(Collectors.toList());

        return new BodyFailedResponse(errors);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public BodyFailedResponse errorFormBind(MethodArgumentNotValidException $e) {
        final var errors = $e.getBindingResult()
            .getFieldErrors()
            .stream()
            .map(ApiError::getInstance)
            .collect(Collectors.toList());
        return new BodyFailedResponse(errors);
    }

    @ExceptionHandler(ResourceBodyException.class)
    public BodyFailedResponse errorFormMap(final ResourceBodyException $e) {
        return new BodyFailedResponse($e.getErrors());
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public PathFailedResponse errorPath(ResourceNotFoundException $e) {
        return new PathFailedResponse($e.getMessage());
    }

    @ExceptionHandler(MultipartException.class)
    public BodyFailedResponse handleFileUpload(MultipartException e) {
        e.printStackTrace();
        final var apiError = new ApiError(
                ValidationErrorCode.INVALID_FIELD,
                "Please use contentType multipart/form-data",
                PathError.of("contentType")
        );
        return new BodyFailedResponse(apiError);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public BodyFailedResponse handleFileUpload(HttpMediaTypeNotSupportedException e) {
        e.printStackTrace();
        final var apiError = new ApiError(
                ValidationErrorCode.INVALID_FIELD,
                "Please use contentType multipart/form-data",
                PathError.of("contentType")
        );
        return new BodyFailedResponse(apiError);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<String> handleAuthenticationException(AccessDeniedException e) {
        final var response = "{\"error\":\"access denied\",\"error_description\":\"unauthorized permission please use your role nob :D\"}";
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    public BodyFailedResponse handleFileUpload(Exception e) {
        final var apiError = new ApiError(
                ValidationErrorCode.INTERNAL_ERROR,
                e.getMessage(),
                PathError.of("SERVER_ERROR")
        );
        return new BodyFailedResponse(apiError);
    }

//    @ExceptionHandler(MultipartException.class)
//    public BodyFailedResponse handleFileUpload(MaxUploadSizeExceededException $e, HttpServletRequest $request, Exception e, HttpServletRequest ks) {
//        final var apiError = new ApiError(
//                ValidationErrorCode.EXCEED,
//                "The field files exceeds its maximum permitted size",
//                PathError.of("files")
//        );
//        return new BodyFailedResponse(apiError);
//    }
}
