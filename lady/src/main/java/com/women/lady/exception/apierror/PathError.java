package com.women.lady.exception.apierror;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@AllArgsConstructor(staticName = "of")
@Getter
public class PathError
{
    @NotNull
    private String path;
}
