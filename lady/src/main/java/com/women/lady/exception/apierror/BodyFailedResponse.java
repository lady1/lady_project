package com.women.lady.exception.apierror;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class BodyFailedResponse extends ResponseEntity<BodyFailedResponse.Payload> {

    public BodyFailedResponse(@NotNull final List<ApiError> $e)
    {
        super(new Payload(HttpStatus.BAD_REQUEST.value(), errorViews($e)), HttpStatus.BAD_REQUEST);
    }

    public BodyFailedResponse(@NotNull final ApiError $e)
    {
        super(new Payload(HttpStatus.BAD_REQUEST.value(), errorViews(Arrays.asList($e))), HttpStatus.BAD_REQUEST);
    }

    private static List<ApiErrorView> errorViews(List<ApiError> _errors)
    {
        return _errors.stream().map(ApiErrorView::new).collect(Collectors.toList());
    }

    @Value
    @AllArgsConstructor
    @EqualsAndHashCode
    public static class Payload
    {
        private Payload()
        {
            this.status = null;
            this.errors = Collections.emptyList();
        }
        @NonNull
        private Integer status;
        @NonNull
        private List<ApiErrorView> errors;
    }
}
