package com.women.lady.exception;

import com.women.lady.common.ApiResponse;

public class ApiBaseException extends Exception {
    protected ApiResponse response;
    public ApiBaseException(Exception e, ApiResponse response){
        super(e);
        this.response = response;
    }
    public ApiBaseException(String code, ApiResponse response){
        super(code);
        this.response = response.fail(code);
    }
    public ApiBaseException(String code, String message, ApiResponse response){
        super(code);
        this.response = response.fail(code,message);
    }
}
