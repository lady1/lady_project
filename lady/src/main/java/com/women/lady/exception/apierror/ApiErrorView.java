package com.women.lady.exception.apierror;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

@Value
@EqualsAndHashCode
public class ApiErrorView
{
    @NonNull
    private String code;
    @NonNull
    private String message;

    private String location;

    public ApiErrorView(@NonNull final ApiError _error)
    {
        this.code = _error.getCode().toString();
        this.message = _error.getMessage();

        if (_error.getPathError().isPresent()) {
            PathError pathError = _error.getPathError().get();
            this.location = pathError.getPath();
        } else {
            this.location = null;
        }

    }
}
