package com.women.lady.exception.apierror;

public enum  ValidationErrorCode {
    MISSING_REFERENCE,
    MISSING_FIELD,
    INVALID_FIELD,
    DUPLICATE,
    EXCEED,
    INTERNAL_ERROR;
}
