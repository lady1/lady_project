package com.women.lady.exception.Resource;

import com.women.lady.exception.apierror.ApiError;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

public class ResourceBodyException extends Exception {

    private List<ApiError> errors;

    public ResourceBodyException(@NotNull final List<ApiError> $errors)
    {
        this.errors = $errors;
    }

    public ResourceBodyException(@NotNull final ApiError $errors)
    {
        this.errors = Arrays.asList($errors);
    }

    public List<ApiError> getErrors()
    {
        return this.errors;
    }
}
