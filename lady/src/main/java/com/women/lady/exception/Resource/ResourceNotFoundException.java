package com.women.lady.exception.Resource;

import javax.validation.constraints.NotNull;

public class ResourceNotFoundException extends Exception {
    public ResourceNotFoundException(@NotNull final String message) {
        super(message);
    }
}
