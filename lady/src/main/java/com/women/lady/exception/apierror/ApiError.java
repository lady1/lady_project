package com.women.lady.exception.apierror;

import lombok.Getter;
import lombok.NonNull;
import lombok.Value;
import org.springframework.validation.FieldError;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Value
public class ApiError
{
    @NonNull
    @Getter
    private ValidationErrorCode code;

    @NonNull
    @Getter
    private String message;

    private PathError pathError;

    public ApiError(@NonNull final ValidationErrorCode _code, @NonNull final String _message, @NonNull final PathError _pathError)
    {
        this.code = _code;
        this.message = _message;
        this.pathError = _pathError;
    }

    public Optional<PathError> getPathError()
    {
        return Optional.ofNullable(this.pathError);
    }

    public static ApiError getInstance(@NotNull final FieldError fieldError) {
        return new ApiError(
            ValidationErrorCode.INVALID_FIELD,
            fieldError.getDefaultMessage(),
            PathError.of(fieldError.getField())
        );
    }
}
