package com.women.lady.graphqlgenerate.SchemaConfiguration;

import com.introproventures.graphql.jpa.query.autoconfigure.GraphQLJpaQueryProperties;
import com.introproventures.graphql.jpa.query.autoconfigure.GraphQLSchemaConfigurer;
import com.introproventures.graphql.jpa.query.autoconfigure.GraphQLShemaRegistration;
import com.introproventures.graphql.jpa.query.schema.*;
import com.introproventures.graphql.jpa.query.schema.impl.GraphQLJpaSchemaBuilder;
import com.women.lady.graphqlgenerate.SchemaConfiguration.graphqlQuery.feed.schemabuilder.FeedWithPoliciesSchemaBuilder;
import com.women.lady.graphqlgenerate.SchemaConfiguration.graphqlQuery.me.schemabuilder.MeSchemaBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@Configuration
public class GraphQLJpaQueryAutoConfiguration implements GraphQLSchemaConfigurer{

    private final GraphQLJpaQueryProperties properties;
    private final EntityManager entityManager;
    private final FeedWithPoliciesSchemaBuilder feedWithPoliciesSchemaBuilder;
    private final MeSchemaBuilder meSchemaBuilder;

    public GraphQLJpaQueryAutoConfiguration(final GraphQLJpaQueryProperties properties,
                                            final EntityManager entityManager,
                                            final FeedWithPoliciesSchemaBuilder feedWithPoliciesSchemaBuilder,
                                            final MeSchemaBuilder meSchemaBuilder) {
        this.properties = properties;
        this.entityManager = entityManager;
        this.feedWithPoliciesSchemaBuilder = feedWithPoliciesSchemaBuilder;
        this.meSchemaBuilder = meSchemaBuilder;
    }

    @Override
    public void configure(final GraphQLShemaRegistration registry) {
        registry.register(graphQLSchemaBuilder()
                .build());

        registry.register(feedWithPoliciesSchemaBuilder.buildSchema());
        registry.register(meSchemaBuilder.buildSchema());
    }

    @Bean
    public GraphQLSchemaBuilder graphQLSchemaBuilder() {
        return new GraphQLJpaSchemaBuilder(entityManager);
    }
}