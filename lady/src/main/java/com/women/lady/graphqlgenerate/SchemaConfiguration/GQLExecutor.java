package com.women.lady.graphqlgenerate.SchemaConfiguration;

import com.introproventures.graphql.jpa.query.schema.GraphQLExecutor;
import com.introproventures.graphql.jpa.query.schema.GraphQLExecutorContext;
import com.introproventures.graphql.jpa.query.schema.GraphQLExecutorContextFactory;
import com.women.lady.graphqlgenerate.SchemaConfiguration.instrumentation.PersistenceFilterInstrumentation;
import com.women.lady.model.security.User;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.execution.instrumentation.ChainedInstrumentation;
import graphql.execution.instrumentation.Instrumentation;
import graphql.schema.GraphQLSchema;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class GQLExecutor implements GraphQLExecutor {

    private final GraphQLSchema graphQLSchema;
    private final GraphQLExecutorContextFactory contextFactory;
    private final Instrumentation instrumentation;

    public GQLExecutor(final GraphQLSchema graphQLSchema,
                       final GraphQLExecutorContextFactory contextFactory,
                       final PersistenceFilterInstrumentation $persistenceFilterInstrumentation) {
        this.instrumentation = new ChainedInstrumentation(
            List.of(
                    $persistenceFilterInstrumentation
            )
        );
        this.graphQLSchema = graphQLSchema;
        this.contextFactory = contextFactory;
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public ExecutionResult execute(String query) {
        return execute(query, Collections.emptyMap());
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public ExecutionResult execute(String query, Map<String, Object> arguments) {
        Map<String, Object> variables = Optional.ofNullable(arguments)
                .orElseGet(Collections::emptyMap);

        GraphQLExecutorContext executorContext = contextFactory.newExecutorContext(graphQLSchema);

        ExecutionInput.Builder executionInput = executorContext.newExecutionInput()
                .query(query)
                .context(resolveContext())
                .variables(variables);
        GraphQL.Builder graphQL = executorContext
                .newGraphQL()
                .instrumentation(instrumentation);
        return graphQL.build()
                .execute(executionInput);
    }

    private User resolveContext() {
        return User.getUserInfo().getUserDetails();
    }
}
