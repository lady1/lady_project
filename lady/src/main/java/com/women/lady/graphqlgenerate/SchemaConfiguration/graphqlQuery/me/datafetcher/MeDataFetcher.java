package com.women.lady.graphqlgenerate.SchemaConfiguration.graphqlQuery.me.datafetcher;

import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.service.UserService;
import graphql.schema.DataFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MeDataFetcher {

    @Autowired
    private UserService userService;

    public DataFetcher<Object> me() {
        return environment -> userService.me();
    }
}
