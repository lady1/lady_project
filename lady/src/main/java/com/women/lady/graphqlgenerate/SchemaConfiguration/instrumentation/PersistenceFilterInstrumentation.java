package com.women.lady.graphqlgenerate.SchemaConfiguration.instrumentation;

import com.women.lady.model.security.Authority;
import com.women.lady.model.security.User;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.execution.instrumentation.parameters.InstrumentationFieldFetchParameters;
import graphql.schema.DataFetcher;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@Component
public class PersistenceFilterInstrumentation extends SimpleInstrumentation {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    private static final String USER_FILTER = "userFilter";
    private static final String PRODUCER_FILTER = "producerFilter";
    private static final String ADMIN_FILTER = "adminFilter";
    private static final String DELIVERY_FILTER = "deliveryFilter";

    private static final String ROLE_ADMIN = "ADMIN";
    private static final String ROLE_USER = "USER";
    private static final String ROLE_PRODUCER = "PRODUCER";
    private static final String ROLE_DELIVERY = "DELIVERY";

    @Override
    public DataFetcher<?> instrumentDataFetcher(final DataFetcher<?> dataFetcher, final InstrumentationFieldFetchParameters parameters) {
        final var user = (User) parameters.getExecutionContext().getContext();
        final var session = entityManager.unwrap(Session.class);
        for (Authority authority : user.getAuthorities()) {
            if (authority.getName().equals(ROLE_USER)) {
                session.enableFilter(USER_FILTER).setParameter("id", user.getId());
            } else if (authority.getName().equals(ROLE_PRODUCER)) {
                session.enableFilter(PRODUCER_FILTER).setParameter("id", user.getId());
            } else if (authority.getName().equals(ROLE_DELIVERY)) {
                session.enableFilter(DELIVERY_FILTER);
            } else if (authority.getName().equals(ROLE_ADMIN)) {
                session.enableFilter(ADMIN_FILTER);
            } else {
                throw new RuntimeException("anonymous user.");
            }
        }
        return super.instrumentDataFetcher(dataFetcher, parameters);
    }
}
