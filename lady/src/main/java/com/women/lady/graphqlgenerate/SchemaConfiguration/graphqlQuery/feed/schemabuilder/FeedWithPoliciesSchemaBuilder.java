package com.women.lady.graphqlgenerate.SchemaConfiguration.graphqlQuery.feed.schemabuilder;

import com.women.lady.graphqlgenerate.SchemaConfiguration.graphqlQuery.SchemaBuilderHelper;
import com.women.lady.graphqlgenerate.SchemaConfiguration.graphqlQuery.feed.datafetcher.FeedWithPoliciesDataFetcher;
import graphql.schema.idl.RuntimeWiring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FeedWithPoliciesSchemaBuilder extends SchemaBuilderHelper {

    @Autowired
    private FeedWithPoliciesDataFetcher FeedWithPoliciesDataFetcher;

    private static final String PATH = "/gql/feedWithPolicies.graphqls";

    public FeedWithPoliciesSchemaBuilder() {
        super(PATH);
    }

    @Override
    protected RuntimeWiring buildWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("Query", typeWiring ->
                        typeWiring
                                .dataFetcher("feedWithPolicies", FeedWithPoliciesDataFetcher.get())
                )
                .build();
    }
}
