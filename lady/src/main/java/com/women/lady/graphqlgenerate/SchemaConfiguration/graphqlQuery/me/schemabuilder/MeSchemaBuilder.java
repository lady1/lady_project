package com.women.lady.graphqlgenerate.SchemaConfiguration.graphqlQuery.me.schemabuilder;

import com.women.lady.graphqlgenerate.SchemaConfiguration.graphqlQuery.SchemaBuilderHelper;
import com.women.lady.graphqlgenerate.SchemaConfiguration.graphqlQuery.me.datafetcher.MeDataFetcher;
import graphql.schema.idl.RuntimeWiring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MeSchemaBuilder extends SchemaBuilderHelper {

    @Autowired
    private MeDataFetcher meDataFetcher;

    private static final String PATH = "/gql/me.graphqls";

    public MeSchemaBuilder() {
        super(PATH);
    }

    @Override
    protected RuntimeWiring buildWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("Query", typeWiring ->
                        typeWiring
                                .dataFetcher("me", meDataFetcher.me())
                )
                .build();
    }
}
