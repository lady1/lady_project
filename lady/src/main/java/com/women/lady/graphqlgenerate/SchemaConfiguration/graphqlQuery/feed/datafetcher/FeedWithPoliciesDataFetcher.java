package com.women.lady.graphqlgenerate.SchemaConfiguration.graphqlQuery.feed.datafetcher;

import com.women.lady.service.FeedService;
import graphql.schema.DataFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class FeedWithPoliciesDataFetcher {

    @Autowired
    private FeedService feedService;





    public DataFetcher<Object> get() {
        return environment -> {
            final var page = (Map<String, Object>) environment.getArgument("page");
            var start = 0;
            var limit = 20;
            if (page != null) {
                start = (int)(page.get("start") == null ? 0 : page.get("start"));
                limit = (int)(page.get("limit") == null ? 10 : page.get("limit"));
            }
            final var feeds = feedService.getFeedWithPolicies(start, limit);
            final var result = new HashMap<String, Object>();
            result.put("total", feeds.getTotalElements());
            result.put("page", feeds.getTotalPages());
            result.put("feeds", feeds.getContent());
            return result;
        };
    }
}
