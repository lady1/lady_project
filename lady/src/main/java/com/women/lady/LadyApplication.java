package com.women.lady;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableAutoConfiguration(exclude = {
		FlywayAutoConfiguration.class
})
public class LadyApplication {

	public static void main(String[] args) {
		SpringApplication.run(LadyApplication.class, args);
	}

}
