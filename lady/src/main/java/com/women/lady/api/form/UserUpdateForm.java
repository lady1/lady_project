package com.women.lady.api.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import java.time.LocalDate;

@Data
public class UserUpdateForm {

    @Email
    private String email;

    @Length(max = 255)
    private String username;

    @Length(min = 8, max = 12)
    private String password;

    private String Bio;

    @Length(max = 50)
    private String contact;

    private String address;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;
}
