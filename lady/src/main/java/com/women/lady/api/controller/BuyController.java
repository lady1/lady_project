package com.women.lady.api.controller;

import com.women.lady.api.form.BuyCreateForm;
import com.women.lady.api.form.BuyRejectForm;
import com.women.lady.common.ResourceId;
import com.women.lady.configure.CustomPermissionEvaluator;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.service.BuyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class BuyController {

	@Autowired
	private BuyService buyService;

	@PreAuthorize(CustomPermissionEvaluator.USER_AUTHORIZE)
	@PostMapping("/feed/{id}/buy")
	public ResponseEntity<ResourceId> buy(
			@RequestBody @Valid final BuyCreateForm form,
			@PathVariable("id") final String id) throws ResourceNotFoundException, ResourceBodyException {

		try {
			final var feedId = Long.valueOf(id);
			return ResponseEntity.ok(buyService.buy(form, feedId));
		} catch (final NumberFormatException e) {
			throw new ResourceNotFoundException("Product id dose not exist");
		}
	}

	@PreAuthorize(CustomPermissionEvaluator.USER_AUTHORIZE)
	@DeleteMapping("/feed/buy/{id}")
	public ResponseEntity<ResourceId> cancel(@PathVariable("id") final String id) throws ResourceNotFoundException, ResourceBodyException {

		try {
			final var buyId = Long.valueOf(id);
			return ResponseEntity.ok(buyService.cancelBuy(buyId));
		} catch (final NumberFormatException e) {
			throw new ResourceNotFoundException("Product id dose not exist");
		}
	}

	@PreAuthorize(CustomPermissionEvaluator.PRODUCER_AUTHORIZE)
	@PostMapping("/feed/buy/{id}/accept")
	public ResponseEntity<ResourceId> accept(@PathVariable("id") final String id) throws ResourceNotFoundException, ResourceBodyException {

		try {
			final var buyId = Long.valueOf(id);
			return ResponseEntity.ok(buyService.producerAccept(buyId));
		} catch (final NumberFormatException e) {
			throw new ResourceNotFoundException("Product id dose not exist");
		}
	}

	@PreAuthorize(CustomPermissionEvaluator.PRODUCER_AUTHORIZE)
	@PostMapping("/feed/buy/{id}/reject")
	public ResponseEntity<ResourceId> reject(@PathVariable("id") final String id,
	                                         @RequestBody @Valid final BuyRejectForm $form) throws ResourceNotFoundException, ResourceBodyException {

		try {
			final var buyId = Long.valueOf(id);
			return ResponseEntity.ok(buyService.producerReject(buyId, $form));
		} catch (final NumberFormatException e) {
			throw new ResourceNotFoundException("Product id dose not exist");
		}
	}

	@PreAuthorize(CustomPermissionEvaluator.ADMIN_AUTHORIZE)
	@PostMapping("/feed/buy/{id}/assign/{deliverId}")
	public ResponseEntity<ResourceId> assignDeliver(@PathVariable("id") final String $id,
                                                    @PathVariable("deliverId") String $deliverId) throws ResourceNotFoundException, ResourceBodyException {

		try {
			final var buyId = Long.valueOf($id);
            final var deliverId = Long.valueOf($deliverId);
			return ResponseEntity.ok(buyService.assignDeliver(buyId, deliverId));
		} catch (final NumberFormatException e) {
			throw new ResourceNotFoundException("Product id dose not exist");
		}
	}

	@PreAuthorize(CustomPermissionEvaluator.ADMIN_AUTHORIZE)
	@PostMapping("/feed/buy/{id}/successfulByAdmin")
	public ResponseEntity<ResourceId> deliverSuccessfulByAdmin(@PathVariable("id") final String $id) throws ResourceNotFoundException, ResourceBodyException {

		try {
			final var buyId = Long.valueOf($id);
			return ResponseEntity.ok(buyService.deliverSuccessfulByAdmin(buyId));
		} catch (final NumberFormatException e) {
			throw new ResourceNotFoundException("Product id dose not exist");
		}
	}
	// change in other branch

	@PreAuthorize(CustomPermissionEvaluator.PRODUCER_AUTHORIZE)
	@PostMapping("/feed/buy/{id}/successful")
	public ResponseEntity<ResourceId> deliverSuccessful(@PathVariable("id") final String $id) throws ResourceNotFoundException, ResourceBodyException {

		try {
			final var buyId = Long.valueOf($id);
			return ResponseEntity.ok(buyService.deliverSuccessful(buyId));
		} catch (final NumberFormatException e) {
			throw new ResourceNotFoundException("Product id dose not exist");
		}
	}
}
