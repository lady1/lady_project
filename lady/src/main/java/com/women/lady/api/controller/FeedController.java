package com.women.lady.api.controller;

import com.women.lady.api.form.FeedUpdateForm;
import com.women.lady.common.ResourceId;
import com.women.lady.configure.CustomPermissionEvaluator;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.api.form.FeedCreateForm;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.exception.apierror.ApiError;
import com.women.lady.exception.apierror.PathError;
import com.women.lady.exception.apierror.ValidationErrorCode;
import com.women.lady.service.FeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class FeedController
{
    @Autowired
    private FeedService feedService;

    @PreAuthorize(CustomPermissionEvaluator.PRODUCER_AUTHORIZE)
    @PostMapping(value = "/feed", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ResourceId> create(
            @Valid FeedCreateForm $feedCreateForm,
            @RequestParam(value = "files") List<MultipartFile> $files
    ) throws ResourceBodyException, ResourceNotFoundException {
        if ($files == null || $files.isEmpty()) {
            throw new ResourceBodyException(
                   new ApiError(
                           ValidationErrorCode.MISSING_FIELD,
                           "file not be null",
                           PathError.of("feed")
                   )
            );
        }
        return ResponseEntity.ok(feedService.create($feedCreateForm, $files));
    }

    @PreAuthorize(CustomPermissionEvaluator.PRODUCER_AUTHORIZE)
    @PutMapping(value = "/feed/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ResourceId> update(
            @Valid FeedUpdateForm $feedUpdateForm,
            @RequestParam(value = "files") List<MultipartFile> $files,
            @PathVariable("id") String id
    ) throws ResourceBodyException, ResourceNotFoundException {
        try {
            final var feedId = Long.valueOf(id);
            return ResponseEntity.ok(feedService.update($feedUpdateForm, $files, feedId));
        } catch (NumberFormatException e) {
            throw new ResourceNotFoundException(String.format("id not found %s", id));
        }
    }

    @PreAuthorize(CustomPermissionEvaluator.PRODUCER_AUTHORIZE)
    @DeleteMapping("/feed/{id}")
    public ResponseEntity<ResourceId> delete(
            @PathVariable("id") String id
    ) throws ResourceNotFoundException {
        try {
            final var feedId = Long.valueOf(id);
            return ResponseEntity.ok(feedService.delete(feedId));
        } catch (NumberFormatException e) {
            throw new ResourceNotFoundException(String.format("id not found %s", id));
        }
    }
}
