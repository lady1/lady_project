package com.women.lady.api.form;

import com.women.lady.entities.feed.ColorEnum;
import lombok.Data;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class BuyCreateForm {

    @NotNull
    @Valid
    @Size(max = 10)
    private List<Set> sets;

    @Valid
    @NotNull
    private LocationCreateForm location;


    @Getter
    public static class Set{

        @NotNull
        @Max(1000)
        private Integer unit;

        @NotNull
        private ColorEnum color;
    }
}
