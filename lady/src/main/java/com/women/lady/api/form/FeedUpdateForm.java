package com.women.lady.api.form;

import com.women.lady.entities.Categories;
import com.women.lady.entities.feed.ColorEnum;
import com.women.lady.entities.feed.SizeEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Data
public class FeedUpdateForm
{
    @Size(max = 200, min = 10)
    private String brand;

    private String description;

    @Max(1000)
    @Min(0)
    private Float price;

    private Boolean promotion;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate expirationDate;

    @Valid
    private LocationCreateForm location;

    private Categories categories;

    private Boolean outOfStock;

    @Size(max = 10)
    private List<ColorEnum> colors;

    @Size(max = 10)
    private List<SizeEnum> sizes;
}
