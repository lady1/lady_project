package com.women.lady.api.controller;

import com.women.lady.api.form.FeedCreateForm;
import com.women.lady.api.form.SlideCreateForm;
import com.women.lady.common.ResourceId;
import com.women.lady.configure.CustomPermissionEvaluator;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.exception.apierror.ApiError;
import com.women.lady.exception.apierror.PathError;
import com.women.lady.exception.apierror.ValidationErrorCode;
import com.women.lady.service.SlideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/slide")
public class SlideController {

    @Autowired
    private SlideService slideService;

    @PreAuthorize(CustomPermissionEvaluator.ADMIN_AUTHORIZE)
    @PostMapping
    public ResponseEntity<ResourceId> add(
            @RequestParam(value = "file") MultipartFile $file,
            @Valid SlideCreateForm slideCreateForm
    ) throws ResourceBodyException {
        if ($file == null) {
            throw new ResourceBodyException(new ApiError(ValidationErrorCode.MISSING_FIELD, "file not be null", PathError.of("feed")));
        }
        return ResponseEntity.ok(slideService.add($file, slideCreateForm.getIndex()));
    }

    @PreAuthorize(CustomPermissionEvaluator.ADMIN_AUTHORIZE)
    @DeleteMapping("/{id}")
    public ResponseEntity<ResourceId> remove(@PathVariable("id") final Long $id) throws ResourceNotFoundException {
        return ResponseEntity.ok(slideService.remove($id));
    }
}
