package com.women.lady.api.form;

import lombok.Data;

@Data
public class SlideCreateForm {

    private Integer index;
}
