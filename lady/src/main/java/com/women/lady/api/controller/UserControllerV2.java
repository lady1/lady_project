package com.women.lady.api.controller;

import com.women.lady.api.form.UserCreateForm;
import com.women.lady.api.form.UserUpdateForm;
import com.women.lady.api.form.UserUpdateFormAdmin;
import com.women.lady.common.ResourceId;
import com.women.lady.configure.CustomPermissionEvaluator;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class UserControllerV2 {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/user", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ResourceId> create(
            @Valid UserCreateForm $createForm,
            @RequestParam(value = "profile", required = false) MultipartFile $profile,
            @RequestParam(value = "cover", required = false) MultipartFile $cover
    ) throws ResourceBodyException {
        return ResponseEntity.ok(userService.create($createForm, $profile, $cover));
    }

    @PreAuthorize(CustomPermissionEvaluator.ADMIN_AUTHORIZE)
    @PostMapping(value = "/user/admin", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ResourceId> createAdmin(
            @Valid UserCreateForm $createForm,
            @RequestParam(value = "profile", required = false) MultipartFile $profile,
            @RequestParam(value = "cover", required = false) MultipartFile $cover
    ) throws ResourceBodyException {
        return ResponseEntity.ok(userService.create($createForm, $profile, $cover));
    }

    @PutMapping(value = "/user", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ResourceId> update(
            @Valid UserUpdateForm $form,
            @RequestParam(value = "profile", required = false) MultipartFile $profile,
            @RequestParam(value = "cover", required = false) MultipartFile $cover
    ) throws ResourceBodyException, ResourceNotFoundException {
        return ResponseEntity.ok(userService.update($form, $profile, $cover));
    }

    @PreAuthorize(CustomPermissionEvaluator.ADMIN_AUTHORIZE)
    @PutMapping(value = "/user/{id}/admin", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ResourceId> updateUserByAdmin(
            @Valid UserUpdateFormAdmin $form,
            @RequestParam(value = "profile", required = false) MultipartFile $profile,
            @RequestParam(value = "cover", required = false) MultipartFile $cover,
            @PathVariable("id") Long id
    ) throws ResourceBodyException, ResourceNotFoundException {
        return ResponseEntity.ok(userService.updateByAdmin($form, $profile, $cover, id));
    }

    @PreAuthorize(CustomPermissionEvaluator.ADMIN_AUTHORIZE)
    @DeleteMapping(value = "/user/{id}/admin")
    public ResponseEntity<ResourceId> deleteUserByAdmin(@PathVariable("id") Long id) throws ResourceBodyException, ResourceNotFoundException {
        return ResponseEntity.ok(userService.delete(id));
    }

//    @GetMapping(value = "/me")
//    public ResponseEntity<ResourceId> me() throws ResourceBodyException, ResourceNotFoundException {
//        return ResponseEntity.ok(userService.delete(id));
//    }
}
