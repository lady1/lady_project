package com.women.lady.api.controller;

import com.women.lady.common.ApiRequest;
import com.women.lady.common.ApiResponse;
import com.women.lady.exception.ApiBaseException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//@RestController
//@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserManagementService userManagementService;

    @PostMapping("/user/create")
    public @ResponseBody
    ApiResponse createUser(@RequestBody ApiRequest apiRequest) throws ApiBaseException {
        return userManagementService.createOrUpdateUser(apiRequest);
    }

    @PostMapping("/user/update")
    public @ResponseBody
    ApiResponse updateUser(@RequestBody ApiRequest apiRequest) throws ApiBaseException {
        return userManagementService.createOrUpdateUser(apiRequest);
    }

    @GetMapping("/user/{id}")
    public @ResponseBody
    ApiResponse findUserById(@PathVariable("id") Integer id) {
        return userManagementService.findUserById(id);
    }

    @GetMapping("/user/detail/{id}")
    public @ResponseBody
    ApiResponse findUserDetail(@PathVariable("id") Integer id) {
        return userManagementService.findUserDetail(id);
    }

    @PostMapping("/user/all")
    public @ResponseBody
    ApiResponse findAllUser(@RequestBody ApiRequest apiRequest) throws ApiBaseException {
        return userManagementService.findAllUser(apiRequest);
    }

    @PostMapping("/user/manage_status")
    public @ResponseBody
    ApiResponse manageUserStatus(@RequestBody ApiRequest apiRequest) throws ApiBaseException, ResourceNotFoundException {
        return userManagementService.manageUserStatus(apiRequest);
    }


}
