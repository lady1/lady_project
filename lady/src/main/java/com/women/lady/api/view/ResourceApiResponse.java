package com.women.lady.api.view;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(staticName = "of")
public class ResourceApiResponse
{
    private Long resourceId;
}
