package com.women.lady.api.controller;

import com.women.lady.common.ResourceId;
import com.women.lady.configure.CustomPermissionEvaluator;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class FollowController {

    @Autowired
    private UserService userService;

    @PreAuthorize(CustomPermissionEvaluator.PRODUCER_USER_AUTHORIZE)
    @PostMapping("/follow/{id}")
    public ResourceId follow(@PathVariable("id") Long $id) throws ResourceNotFoundException, ResourceBodyException {
        return userService.follow($id);
    }

    @PreAuthorize(CustomPermissionEvaluator.PRODUCER_USER_AUTHORIZE)
    @DeleteMapping("/follow/{id}")
    public ResourceId unFollow(@PathVariable("id") Long $id) throws ResourceNotFoundException {
        return userService.unFollow($id);
    }
}
