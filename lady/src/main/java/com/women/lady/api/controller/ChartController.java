package com.women.lady.api.controller;

import com.women.lady.common.ResourceId;
import com.women.lady.configure.CustomPermissionEvaluator;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.service.ChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ChartController {

    @Autowired
    private ChartService chartService;

    @PreAuthorize(CustomPermissionEvaluator.USER_AUTHORIZE)
    @PostMapping("/feed/{id}/chart")
    public ResponseEntity<ResourceId> addChart(@PathVariable("id") String id) throws ResourceNotFoundException, ResourceBodyException {
        try {
            final var feedId = Long.valueOf(id);
            return ResponseEntity.ok(chartService.addChart(feedId));
        } catch (NumberFormatException e) {
            throw new ResourceNotFoundException("feed id dose not exist");
        }
    }

    @PreAuthorize(CustomPermissionEvaluator.USER_AUTHORIZE)
    @DeleteMapping("/feed/{id}/chart")
    public ResponseEntity<ResourceId> removeChart(@PathVariable("id") String id) throws ResourceNotFoundException {
        try {
            final var feedId = Long.valueOf(id);
            return ResponseEntity.ok(chartService.removeChart(feedId));
        } catch (NumberFormatException e) {
            throw new ResourceNotFoundException("product id dose not exist");
        }
    }
}
