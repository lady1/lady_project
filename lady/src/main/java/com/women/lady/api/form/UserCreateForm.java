package com.women.lady.api.form;

import lombok.Data;
import lombok.Value;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class UserCreateForm {

    @Email
    private String email;

    @Length(min = 8, max = 50)
    @NotNull
    private String username;

    @NotNull
    @Length(min = 8, max = 12)
    private String password;

    private String Bio;

    @Length(max = 50)
    @NotNull
    private String contact;

    private String address;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;

    @NotNull
    private String authorities;
}
