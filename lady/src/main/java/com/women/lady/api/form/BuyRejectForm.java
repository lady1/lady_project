package com.women.lady.api.form;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class BuyRejectForm {

    @NotNull
    private String reason;
}
