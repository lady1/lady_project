package com.women.lady.api.form;

import lombok.Data;
import lombok.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class LocationCreateForm {

    @NotNull
    @Size(max = 200)
    private String name;

    @NotNull
    private Float latitude;

    @NotNull
    private Float longitude;
}