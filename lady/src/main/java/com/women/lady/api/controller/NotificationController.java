package com.women.lady.api.controller;

import com.women.lady.api.form.BuyCreateForm;
import com.women.lady.api.form.BuyRejectForm;
import com.women.lady.common.ResourceId;
import com.women.lady.configure.CustomPermissionEvaluator;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.Resource.ResourceNotFoundException;
import com.women.lady.service.BuyService;
import com.women.lady.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @PostMapping("/notification/{id}/markAsRead")
    public ResponseEntity<ResourceId> markAsRead(@PathVariable("id") String id) throws ResourceNotFoundException {
        try {
            final var notificationId = Long.valueOf(id);
            return ResponseEntity.ok(notificationService.markAsRead(notificationId));
        } catch (NumberFormatException e) {
            throw new ResourceNotFoundException("notification id not exist");
        }
    }
}
