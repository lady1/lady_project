package com.women.lady.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableResourceServer
public class ResourceServerConfigure extends ResourceServerConfigurerAdapter {

    private static final String RESOURCE_ID = "resource-server-rest-api";
    private static final String SECURED_READ_SCOPE = "#oauth2.hasScope('read')";
    private static final String SECURED_WRITE_SCOPE = "#oauth2.hasScope('write')";
    private static final String PRIVATE_ACCESS_PATTERN = "/api/**";
    private static final String PUBLIC_ACCESS_PATTERN = "/public/**";
    private static final String PUBLIC_CREATE_USER = "/api/user/create";
    private static final String PUBLIC_CREATE_USER_V2 = "/api/user";
    private static final String PUBLIC_TOKEN = "/oauth/token";

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(PUBLIC_ACCESS_PATTERN, PUBLIC_CREATE_USER, PUBLIC_CREATE_USER_V2).permitAll()
                .antMatchers(PRIVATE_ACCESS_PATTERN).hasAnyAuthority("ADMIN", "PRODUCER", "USER", "DELIVER")
                .antMatchers(HttpMethod.POST, PRIVATE_ACCESS_PATTERN).access(SECURED_WRITE_SCOPE).anyRequest().access(SECURED_READ_SCOPE)
        ;
    }
}
