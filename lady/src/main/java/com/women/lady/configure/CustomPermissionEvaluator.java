package com.women.lady.configure;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Arrays;

public class CustomPermissionEvaluator implements PermissionEvaluator {

    public static final String USER_AUTHORIZE = "hasPermission('role', 'USER')";
    public static final String ADMIN_AUTHORIZE = "hasPermission('role', 'ADMIN')";
    public static final String PRODUCER_AUTHORIZE = "hasPermission('role', 'PRODUCER')";
    public static final String PRODUCER_USER_AUTHORIZE = "hasPermission('role', 'PRODUCER,USER')";


    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        if (permission == null){
            return false;
        }
        return hasPrivilege(authentication, permission.toString().toLowerCase());
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        if ((authentication == null) || (targetType == null) || !(permission instanceof String)) {
            return false;
        }
        return hasPrivilege(authentication, permission.toString().toUpperCase());
    }

    private boolean hasPrivilege(Authentication auth, String permission) {
        for (GrantedAuthority grantedAuth : auth.getAuthorities()) {
            final var roles = Arrays.asList(StringUtils.split(permission, ","));
            if (roles.contains(grantedAuth.getAuthority().toLowerCase()))
                    return true;
        }
        return false;
    }
}
