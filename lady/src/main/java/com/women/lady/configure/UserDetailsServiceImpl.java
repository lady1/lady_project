package com.women.lady.configure;

import com.women.lady.model.security.Authority;
import com.women.lady.repository.UserRepository;
import com.women.lady.model.security.User;
import com.women.lady.model.security.UserAuthority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .filter(u -> u.getStatus().equalsIgnoreCase("active"))
                .orElseThrow(() -> new UsernameNotFoundException("User not found with -> username or email : " + username)
        );
        //fix : error AbstractUserDetailsAuthenticationProvider to get authorize because we use fetch lazy.
        final var authorities = user.getAuthorities()
                .stream().map(Authority::copy)
                .collect(Collectors.toSet());
        user.setAuthorities(authorities);

        return new UserAuthority(user);
    }
}
