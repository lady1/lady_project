package com.women.lady;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.SQLException;

@Service
public class UpdateMigrationService {

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    public void upgrade() throws SQLException {
        final var flyway = Flyway.configure()
                .dataSource(dataSource)
                .locations("migration")
                .validateMigrationNaming(false)
                .validateOnMigrate(false)
                .placeholderReplacement(false)
                .outOfOrder(true)
                .baselineOnMigrate(true)
                .load();
        flyway.migrate();
    }
}
