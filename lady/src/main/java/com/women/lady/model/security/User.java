package com.women.lady.model.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.introproventures.graphql.jpa.query.annotation.GraphQLIgnore;
import com.women.lady.api.form.UserCreateForm;
import com.women.lady.api.form.UserUpdateForm;
import com.women.lady.api.form.UserUpdateFormAdmin;
import com.women.lady.entities.buy.Buy;
import com.women.lady.entities.chart.Chart;
import com.women.lady.entities.follow.Follow;
import com.women.lady.entities.media.Media;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.apierror.ApiError;
import com.women.lady.exception.apierror.PathError;
import com.women.lady.exception.apierror.ValidationErrorCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")
@NoArgsConstructor
@Setter
@Getter
@FilterDef(name = "userFilter",
        defaultCondition = "id is null",
        parameters = {@ParamDef(name = "id", type = "long")})
@Filter(name = "userFilter", condition = " EXISTS( SELECT FROM users_authorities as us where us.user_id = id AND us.authority_id IN (2, 3)) ")
@FilterDef(name = "producerFilter",
        defaultCondition = "id is null",
        parameters = {@ParamDef(name = "id", type = "long")})
@Filter(name = "producerFilter", condition = " EXISTS( SELECT FROM users_authorities as us where us.user_id = id AND us.authority_id IN (2, 3)) ")
//@Table(name = "USERS", uniqueConstraints = {@UniqueConstraint(columnNames = {"USER_NAME"})})
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "USER_NAME", length = 20, unique = true)
    private String username;

    @JsonIgnore
    @Column(name = "PASSWORD")
    @GraphQLIgnore
    private String password;

    @Column(name = "CONTACT", length = 50, unique = true)
    private String contact;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "DOB", length = 10)
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

    @Column(name = "BIO")
    private String Bio;

    @Column(name = "STATUS", length = 10)
    private String status;

    @Column(name = "EMAIL", length = 50, unique = true)
    private String email;

    @Column(name = "IS_REQUIRED_CHANGE_PASSWORD")
    private Boolean isRequiredChangePassword;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "profile_id")
    private Media profile;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn( name = "cover_id")
    private Media cover;

    @Column(name = "IS_VISIBLE")
    private Boolean isVisible;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "CREATED_BY")
    private Integer createdBy;

    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    @Column(name = "UPDATED_BY")
    private Integer updatedBy;

    @Column(name = "work")
    private String work;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "married")
    private boolean married;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "request_id")
    private List<Follow> following;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private List<Chart> chart;

    @OneToMany(targetEntity = Buy.class, mappedBy = "userBuy")
    private List<Buy> buys;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "USERS_AUTHORITIES",
            joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "AUTHORITY_ID", referencedColumnName = "ID"))
    @OrderBy
    @JsonIgnore
    //@GraphQLIgnore
    private Set<Authority> authorities = new HashSet<>();

    public static UserAuthority getUserInfo() {
        final var authentication = SecurityContextHolder.getContext().getAuthentication();
        return  (UserAuthority)authentication.getPrincipal();
    }

    public static User map(@NonNull UserCreateForm $createForm) {
        final var user = new User();
        user.setUsername($createForm.getUsername());
        user.setPassword($createForm.getPassword());
        user.setContact($createForm.getContact());
        if ($createForm.getAddress() != null) {
            user.setAddress($createForm.getAddress());
        }
        if ($createForm.getBio() != null) {
            user.setBio($createForm.getBio());
        }
        //fix me: use local date it will get system Default
        if ($createForm.getDateOfBirth() != null) {
            user.setDateOfBirth(Date.from($createForm.getDateOfBirth().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        }
        if ($createForm.getEmail() != null) {
            user.setEmail($createForm.getEmail());
        }
        user.setIsRequiredChangePassword(true);
        user.setCreatedAt(new Date());
        user.setUpdatedAt(new Date());
        return user;
    }

    public static User map(@NonNull User user, @NonNull UserUpdateForm $form) {
        if ($form.getUsername() != null) {
            user.setUsername($form.getUsername());
        }
        if ($form.getAddress() != null) {
            user.setAddress($form.getAddress());
        }
        if ($form.getBio() != null) {
            user.setBio($form.getBio());
        }
        if ($form.getDateOfBirth() != null) {
            final var date = Date.from($form.getDateOfBirth().atStartOfDay(ZoneId.systemDefault()).toInstant());
            user.setDateOfBirth(date);
        }
        if ($form.getContact() != null) {
            user.setContact($form.getContact());
        }
        if ($form.getEmail() != null) {
            user.setEmail($form.getEmail());
        }
        return user;
    }

    public static User map(@NonNull User user, @NonNull UserUpdateFormAdmin $form) throws ResourceBodyException {
        if ($form.getUsername() != null) {
            user.setUsername($form.getUsername());
        }
        if ($form.getAddress() != null) {
            user.setAddress($form.getAddress());
        }
        if ($form.getBio() != null) {
            user.setBio($form.getBio());
        }
        if ($form.getDateOfBirth() != null) {
            final var date = Date.from($form.getDateOfBirth().atStartOfDay(ZoneId.systemDefault()).toInstant());
            user.setDateOfBirth(date);
        }
        if ($form.getContact() != null) {
            user.setContact($form.getContact());
        }
        if ($form.getEmail() != null) {
            user.setEmail($form.getEmail());
        }
        if ($form.getIsVisible() != null) {
            user.setIsVisible($form.getIsVisible());
        }
        if ($form.getStatus() != null) {
            if (!$form.getStatus().equalsIgnoreCase("inactive") &&
                !$form.getStatus().equalsIgnoreCase("active")) {
                throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, "status should be 'inActive' OR 'active' ", PathError.of("user.status")));
            }
            user.setStatus($form.getStatus());
        }
        return user;
    }
}
