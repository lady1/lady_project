package com.women.lady.model.security;

import com.introproventures.graphql.jpa.query.annotation.GraphQLIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="oauth_code")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@GraphQLIgnore
public class OauthCode {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", columnDefinition = "bigint unsigned")
	private Integer id;
	
	@Column(name="code")
	private String code;

//	@Lob
	@Column(name="authentication")
	private byte[] authentication;
}