package com.women.lady.model.security;

import com.introproventures.graphql.jpa.query.annotation.GraphQLIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="oauth_access_token")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@GraphQLIgnore
public class OauthAccessToken {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", columnDefinition = "bigint unsigned")
	private Integer id;
	
	@Column(name="token_id")
	private String tokenId;

//	@Lob
	@Column(name="token")
	private byte[] token;
	
	@Column(name="authentication_id")
	private String authenticationId;
	
	@Column(name="user_name")
	private String userName;
	
	@Column(name="client_id")
	private String clientId;

//	@Lob
	@Column(name="authentication")
	private byte[] authentication;
	
	@Column(name="refresh_token")
	private String refreshToken;
}