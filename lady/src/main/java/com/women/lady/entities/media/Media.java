package com.women.lady.entities.media;

import com.women.lady.common.constraint.AbstractPersistableCustom;
import com.women.lady.model.security.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "media")
@Getter
@Setter
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Media extends AbstractPersistableCustom
{
//    @ManyToOne
//    @JoinColumn(name = "user_id", nullable = false, updatable = false)
//    private User user;

    @Column(name = "name", length = 250)
    private String name;

    @Column(name = "url_images", nullable = false)
    private String urlImages;

    @Column(name = "display_name", length = 1024, nullable = false)
    private String displayName;

    @Column(name = "size", nullable = false)
    private long size;

    @Column(name = "type", length = 50, nullable = false)
    private String type;
}
