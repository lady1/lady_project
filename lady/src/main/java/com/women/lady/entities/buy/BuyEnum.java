package com.women.lady.entities.buy;

public enum BuyEnum {
    PENDING,//first state
    CANCEL,//buyer cancel
    REJECT,//state reject by seller
    ACCEPT,//seller accept
    DELIVERY,//product on the way
    SUCCESSFUL,//buy complete
    BROKEN//product have problem
}
