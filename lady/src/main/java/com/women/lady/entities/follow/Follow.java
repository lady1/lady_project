package com.women.lady.entities.follow;

import com.women.lady.common.constraint.AbstractPersistableCustom;
import com.women.lady.entities.buy.Buy;
import com.women.lady.model.security.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "follow")
@Getter
@Setter
@NoArgsConstructor
@Filter(name = "userFilter", condition = " request_id = :id")
@Filter(name = "producerFilter", condition = " follow_id = :id")
public class Follow extends AbstractPersistableCustom {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn( name = "follow_id")
    private User follow;

    @ManyToOne
    @JoinColumn(name = "request_id")
    private User request;
}
