package com.women.lady.entities.feed;

public enum SizeEnum {
    MINI,
    S,
    M,
    L,
    XL,
    XXL,
    XXX,
    XXXX,
    XXXXX,
}
