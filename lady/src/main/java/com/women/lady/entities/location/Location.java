package com.women.lady.entities.location;

import com.women.lady.api.form.FeedCreateForm;
import com.women.lady.api.form.LocationCreateForm;
import com.women.lady.common.constraint.AbstractPersistableCustom;
import com.women.lady.model.security.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "location")
@Getter
@Setter
public class Location extends AbstractPersistableCustom
{
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    private User user;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "latitude", nullable = false)
    private float latitude;

    @Column(name = "longitude", nullable = false)
    private float longitude;

    public static Location map(@NotNull final LocationCreateForm $form) {
        final var location = new Location();
        location.setUser(User.getUserInfo().getUserDetails());
        location.setName($form.getName());
        location.setLongitude($form.getLongitude());
        location.setLatitude($form.getLatitude());
        location.setCreateDate(new Date());
        location.setUpdateDate(new Date());
        return location;
    }
}
