package com.women.lady.entities.buy;

import com.women.lady.api.form.BuyCreateForm;
import com.women.lady.common.constraint.AbstractPersistableCustom;
import com.women.lady.entities.feed.ColorEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "set")
@Setter
@Getter
public class Set extends AbstractPersistableCustom {

    @NotNull
    @Min(1)
    @Column(name = "unit", nullable = false)
    private Integer unit;

    @Column(name = "color", nullable = false)
    @Enumerated(EnumType.STRING)
    private ColorEnum color;

    public static Set map(@NotNull final BuyCreateForm.Set $set) {
        final var set = new Set();
        set.setUnit($set.getUnit());
        set.setColor($set.getColor());
        return set;
    }
}
