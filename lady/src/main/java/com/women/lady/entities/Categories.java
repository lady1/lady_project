package com.women.lady.entities;

public enum Categories {
    COSMETICS,
    CLOTH,
    TOOL,
    BAG;
}
