package com.women.lady.entities.notification;

import com.women.lady.common.constraint.AbstractPersistableCustom;
import com.women.lady.entities.buy.Buy;
import com.women.lady.entities.feed.Feed;
import com.women.lady.entities.follow.Follow;
import com.women.lady.model.security.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "notification")
@Setter
@Getter
@Filter(name = "userFilter",
        condition = " target_user_id = :id")
        //condition = " EXISTS(SELECT FROM feed f where f.id = feed_id AND f.user_id in (SELECT fo.follow_id FROM follow fo WHERE fo.request_id = :id )) )")
@Filter(name = "producerFilter",
        //condition = " EXISTS(SELECT FROM buy b WHERE b.id = buy_id AND (EXISTS(SELECT FROM feed f where f.id = b.feed_id AND f.user_id = :id))) ")
        condition = " target_user_id = :id ")
@Filter(name = "adminFilter")
@Filter(name = "producerFilter")
public class Notification extends AbstractPersistableCustom {

    @Column(name = "touched")
    private boolean touched;

    @ManyToOne
    @JoinColumn(name = "buy_id")
    private Buy buy;

    @ManyToOne
    @JoinColumn(name = "feed_id")
    private Feed feed;

    @ManyToOne
    @JoinColumn(name = "follow_id")
    private Follow follow;

    @ManyToOne
    @JoinColumn(name = "target_user_id")
    private User targetUser;

    @Column(name = "action_type", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    private NotificationEnum actionType;
}
