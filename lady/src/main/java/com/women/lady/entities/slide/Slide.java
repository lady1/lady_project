package com.women.lady.entities.slide;

import com.women.lady.common.constraint.AbstractPersistableCustom;
import com.women.lady.entities.media.Media;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Table(name = "slide")
@Getter
@Setter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class Slide extends AbstractPersistableCustom {

    @Column(name = "index", nullable = false, unique = true)
    private int index;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "slide_home_id")
    private Media slideHome;
}
