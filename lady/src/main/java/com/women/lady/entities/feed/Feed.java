package com.women.lady.entities.feed;

import com.introproventures.graphql.jpa.query.annotation.GraphQLIgnore;
import com.women.lady.api.form.FeedCreateForm;
import com.women.lady.api.form.FeedUpdateForm;
import com.women.lady.common.StringListConverter;
import com.women.lady.common.constraint.AbstractPersistableCustom;
import com.women.lady.entities.Categories;
import com.women.lady.entities.location.Location;
import com.women.lady.entities.media.Media;
import com.women.lady.exception.Resource.ResourceBodyException;
import com.women.lady.exception.apierror.ApiError;
import com.women.lady.exception.apierror.PathError;
import com.women.lady.exception.apierror.ValidationErrorCode;
import com.women.lady.model.security.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "feed")
@Getter
@Setter
@Accessors(chain = true)
public class Feed extends AbstractPersistableCustom {

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    private User user;

    @Column(name = "brand", nullable = false)
    private String brand;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "expiration_date")
    //@Temporal(TemporalType.DATE)
    private LocalDate expirationDate;

    @Column(name = "description")
    private String description;

    @Column(name = "promotion", columnDefinition = "boolean default false")
    private boolean promotion;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id")
    private Location location;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "feed_id")
    private List<Media> medias;

    @Column(name = "categories", nullable = false)
    @Enumerated(EnumType.STRING)
    private Categories categories;

    @Column(name = "outOfStock")
    private boolean outOfStock;

    @GraphQLIgnore
    @Column(name = "rank")
    private int rank;

    @Convert(converter = StringListConverter.class)
    @Column(name = "colors", columnDefinition = "varchar(255)")
    private List<String> colors;

    @Convert(converter = StringListConverter.class)
    @Column(name = "sizes", columnDefinition = "varchar(255)")
    private List<String> sizes;

    public static Feed map(@NotNull final FeedCreateForm $form) throws ResourceBodyException {
        final var feed = new Feed();
        feed.setUser(User.getUserInfo().getUserDetails());
        feed.setBrand($form.getBrand());
        feed.setPrice(new BigDecimal($form.getPrice()));
        if ($form.getPromotion() != null && $form.getExpirationDate() != null) {
            if ($form.getPromotion()) {
                var t = LocalDate.now();
                if(t.isAfter($form.getExpirationDate())) {
                    throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, "date should be in future", PathError.of("feed/expirationDate")));
                }
                feed.setExpirationDate($form.getExpirationDate());
                feed.setPromotion($form.getPromotion());
            } else {
                throw new ResourceBodyException(new ApiError(ValidationErrorCode.INVALID_FIELD, "promotion should be true when we use expireation date", PathError.of("feed/pomotion")));
            }
        } else if ($form.getPromotion() != null || $form.getExpirationDate() != null) {
            throw new ResourceBodyException(new ApiError(ValidationErrorCode.MISSING_FIELD, "if use promotion should have expired date and promotion", PathError.of("feed/promotion")));
        }

        final var colors = $form.getColors() == null ? null : $form.getColors().stream()
                .map(ColorEnum::toString).collect(Collectors.toList());
        feed.setColors(colors);

        final var sizes = $form.getSizes() == null ? null : $form.getSizes().stream()
                .map(SizeEnum::toString).collect(Collectors.toList());
        feed.setSizes(sizes);

        feed.setOutOfStock($form.isOutOfStock());
        feed.setCategories($form.getCategories());
        feed.setDescription($form.getDescription());
        feed.setCreateDate(new Date());
        feed.setUpdateDate(new Date());
        return feed;
    }

    public static Feed map(@NotNull final Feed $feed,
                           @NotNull final FeedUpdateForm $update) {

        if ($update.getPromotion() != null) {
            $feed.setPromotion($update.getPromotion());
        }
        if ($update.getBrand() != null) {
            $feed.setBrand($update.getBrand());
        }
        if ($update.getPrice() != null) {
            $feed.setPrice(new BigDecimal($update.getPrice()));
        }
        if ($update.getDescription() != null) {
            $feed.setDescription($update.getDescription());
        }
        if ($update.getExpirationDate() != null) {
            $feed.setExpirationDate($update.getExpirationDate());
        }
        if ($update.getOutOfStock() != null) {
            $feed.setOutOfStock($update.getOutOfStock());
        }
        if ($update.getCategories() != null) {
            $feed.setCategories($update.getCategories());
        }

        if ($update.getColors() != null && !$update.getColors().isEmpty()) {
            final var colors = $update.getColors().stream()
                    .map(ColorEnum::toString).collect(Collectors.toList());
            $feed.setColors(colors);
        }

        if ($update.getSizes() != null && !$update.getSizes().isEmpty()) {
            final var sizes = $update.getSizes().stream()
                    .map(SizeEnum::toString).collect(Collectors.toList());
            $feed.setSizes(sizes);
        }

        return $feed;
    }
}
