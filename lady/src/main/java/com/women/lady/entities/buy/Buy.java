package com.women.lady.entities.buy;

import com.women.lady.api.form.BuyCreateForm;
import com.women.lady.api.form.LocationCreateForm;
import com.women.lady.common.constraint.AbstractPersistableCustom;
import com.women.lady.entities.feed.Feed;
import com.women.lady.entities.location.Location;
import com.women.lady.model.security.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "buy")
@Getter
@Setter
@Filter(name = "userFilter", condition = " user_buy_id = :id ")
@Filter(name = "producerFilter", condition = " EXISTS(SELECT FROM feed f where f.id = id AND f.user_id = :id) ")
@Accessors(chain = true)
public class Buy extends AbstractPersistableCustom {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_buy_id")
    private User userBuy;

    @Column(name = "total_amount", nullable = false)
    private BigDecimal totalAmount;

    @OneToOne
    @JoinColumn(name = "feed_id", nullable = false)
    private Feed feed;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private BuyEnum status;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "location_id")
    private Location location;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "buy_id")
    private List<Set> sets;

    @OneToOne
    @JoinColumn(name = "user_delivery_id")
    private User userDelivery;

    @Column(name = "reason", columnDefinition = "text")
    private String reason;

    public static Buy map(@NotNull final Feed $feed,
                          @NotNull final User $user,
                          @NotNull final BuyEnum $status,
                          @NotNull final LocationCreateForm $location,
                          @NotNull final List<BuyCreateForm.Set> $set) {

        final var buy = new Buy();
        buy.setFeed($feed);
        buy.setUserBuy($user);
        buy.setStatus($status);
        final var set = $set.stream().map(Set::map).collect(Collectors.toList());
        buy.setSets(set);
        buy.setLocation(Location.map($location));
        buy.setCreateDate(new Date());
        buy.setUpdateDate(new Date());
        return buy;
    }
}
