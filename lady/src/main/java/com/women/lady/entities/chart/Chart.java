package com.women.lady.entities.chart;

import com.women.lady.common.constraint.AbstractPersistableCustom;
import com.women.lady.entities.feed.Feed;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;

import javax.persistence.*;

@Entity
@Table(name = "chart")
@Setter
@Getter
@Filter(name = "userFilter",
        condition = "user_id = :id"
)
@Filter(name = "adminFilter")
@Filter(name = "deliveryFilter")
public class Chart  extends AbstractPersistableCustom {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "feed_id")
    private Feed feed;
}
