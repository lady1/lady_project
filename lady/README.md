# tambang-core-admin

#curl request
curl --request POST \
  --url http://localhost:8000/oauth/token \
  --header 'authorization: Basic dmVhc25hLWNsaWVudC1pZDphZG1pbjEyMw==' \
  --header 'client_id: veasna-client-id' \
  --header 'content-type: multipart/form-data; boundary=---011000010111000001101001' \
  --header 'grant_type: password' \
  --header 'password: admin123' \
  --header 'username: admin' \
  --cookie JSESSIONID=8E92A0766FFE58484C82F03B3E7C78A2 \
  --form grant_type=password \
  --form username=admin \
  --form password=admin123 \
  --form client_id=veasna-client-id
  
  #Basic Auth
  username:app-client-id
  password:admin123
  
  # Script
insert into public.users
(bio, address, contact, created_at, created_by, dob, email, is_required_change_password, is_visible, password, status, updated_at, updated_by, user_name)
values('work hard work smart!', 'battambang', '', now(), 0, now(), 'admin@lady.com.kh', false, false, '$2a$10$njt0SdDV5vT/xS3laRSt2usTTA0wIZLzX4E3Lz3uK9MyxbEpBTzdW', 'active', now(), 0, 'admin');

insert into oauth_client_details(client_id, resource_ids, client_secret, scope, authorized_grant_types, authorities, access_token_validity, refresh_token_validity) values ('app-client-id', 'resource-server-rest-api', '$2a$10$njt0SdDV5vT/xS3laRSt2usTTA0wIZLzX4E3Lz3uK9MyxbEpBTzdW', 'read,write', 'password,authorization_code,refresh_token,implicit', 'user', 36000, 2592000);

insert into authority(id, name) values (1, 'ADMIN');

insert into authority(id, name) values (2, 'PRODUCER');

insert into authority(id, name) values (3, 'USER');

insert into authority(id, name) values (4, 'DELIVER');

insert into users_authorities(user_id, authority_id) values (1, 1);

